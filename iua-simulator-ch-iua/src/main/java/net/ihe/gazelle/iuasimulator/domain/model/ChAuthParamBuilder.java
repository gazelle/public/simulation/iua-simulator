package net.ihe.gazelle.iuasimulator.domain.model;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 09/04/2023
 */
public class ChAuthParamBuilder extends IuaAuthParamBuilder {

    private final String launch;

    protected ChAuthParamBuilder(ChBuilder builder) {
        super(builder);
        this.launch = builder.launch;
    }

    public static ChBuilder builder() {
        return new ChBuilder();
    }

    public static class ChBuilder extends IuaBuilder<ChBuilder> {

        private String launch;

        public ChBuilder withLaunch(String launch) {
            this.launch = launch;
            return this;
        }
        @Override
        public ChBuilder getThis() {
            return this;
        }

        @Override
        public ChAuthParamBuilder build() { return new ChAuthParamBuilder(this);}
    }

    public String getLaunch() {
        return launch;
    }
}