package net.ihe.gazelle.iuasimulator.interlay.resource;

import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import net.ihe.gazelle.iuasimulator.application.resource.IuaResource;
import net.ihe.gazelle.iuasimulator.client.application.AccessTokenResponseValidation;
import net.ihe.gazelle.iuasimulator.client.application.SamlService;
import net.ihe.gazelle.iuasimulator.client.interlay.keycloak.KeycloakClient;
import net.ihe.gazelle.iuasimulator.domain.model.ChAuthParamBuilder;
import net.ihe.gazelle.iuasimulator.domain.model.ChScopeParamBuilder;
import net.ihe.gazelle.iuasimulator.domain.model.ChTokenParamBuilder;
import net.ihe.gazelle.iuasimulator.interlay.exception.ValidatorErrorMessage;
import net.ihe.gazelle.iuasimulator.interlay.loggingspe.LogAudited;
import net.ihe.gazelle.iuasimulator.interlay.loggingspe.LogLevel;
import net.ihe.gazelle.iuasimulator.interlay.mapper.ChScopeMapper;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.ChAuthQueryParamValidatorImpl;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.ChScopeQueryParamValidatorImpl;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.ChTokenQueryParamValidatorImpl;
import net.ihe.gazelle.iuasimulator.interlay.utils.IuaJsonMetadataUtils;
import java.security.NoSuchAlgorithmException;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 02/02/2023
 */
@LogAudited(LogLevel.INFO)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("ch")
public class ChResourceImpl implements IuaResource<Response> {

    private static final String ENDPOINT_KEY = System.getenv("REALMS_CONF_SPE_IUA_JWT_ENDPOINT");
    private final ChScopeMapper scopeMapper = new ChScopeMapper();
    private final IuaJsonMetadataUtils metadataUtils = new IuaJsonMetadataUtils("ch");
    @Inject
    private AccessTokenResponseValidation accessTokenResponseValidation;
    @Inject
    private SamlService service;
    @Inject
    private ChAuthQueryParamValidatorImpl codeParamValidator;
    @Inject
    private ChScopeQueryParamValidatorImpl scopeParamValidator;
    @Inject
    private ChTokenQueryParamValidatorImpl tokenParamValidator;
    private KeycloakClient client;

    @GET
    @Path("authorize")
    @Override
    public Response getAuthCode(@QueryParam("response_type") String responseType, @QueryParam("client_id") String clientId, @QueryParam("state") String state, @QueryParam("resource") String resource, @QueryParam("code_challenge") String codeChallenge, @QueryParam("code_challenge_method") String codeChallengeMethod, @QueryParam("redirect_uri") String redirectUri, @QueryParam("scope") String scope, @QueryParam("aud") String audience, @QueryParam("launch") String launch) throws NoSuchAlgorithmException {

        client = new KeycloakClient(ENDPOINT_KEY);

        ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder().withResponseType(responseType).withClientId(clientId).withState(state).withResource(resource).withCodeChallenge(codeChallenge).withCodeChallengeMethod(codeChallengeMethod).withRedirectUri(redirectUri).withScope(scope).withAudience(audience).withLaunch(launch).build();

        // validate request
        codeParamValidator.validateCodeAuthorization(chCodeParam);
        scopeParamValidator.validateScope(scopeMapper.getScopeFromQueryParam(scope));

        try {
            return client.requestAuthCodeEndpoint(responseType, clientId, redirectUri, state, scope, audience, launch, codeChallenge, codeChallengeMethod);
        } catch (Exception e) {
            return Response.status(Response.Status.NO_CONTENT).header("Access-Control-Allow-Origin", "*").entity(new ValidatorErrorMessage("Exception Occurred", false)).build();
        }
    }


    @GET
    @Path(".well-known")
    @Override
    public Response getMetadata() {

        String endpoint = System.getenv("IUA_URL");
        try {
            JsonObject json = metadataUtils.generateMetadata(endpoint);
            return Response.accepted().entity(json).header("Access-Control-Allow-Origin", "*").build();
        } catch (Exception e) {
            return Response.noContent().header("Access-Control-Allow-Origin", "*").build();
        }
    }

    @GET
    @Path("certs")
    public Response getCerts() {
        client = new KeycloakClient(ENDPOINT_KEY);
        try {
            return client.requestServerCerts();
        } catch (Exception e) {
            return Response.noContent().header("Access-Control-Allow-Origin", "*").build();
        }
    }

    @GET
    @Path("callback")
    @Override
    public Response retrieveCode(@QueryParam("state") String state, @QueryParam("session_state") String sessionState, @QueryParam("code") String code, @QueryParam("error_code") String errorCode, @QueryParam("error_message") String message) {
        JsonObject json;

        if (null != code) {
            json = Json.createObjectBuilder().add("code", code).build();
            return Response.status(Response.Status.OK).entity(json).header("Access-Control-Allow-Origin", "*").build();
        }
        else if (null != errorCode) {
            json = Json.createObjectBuilder().add("error_message", message).build();
            return Response.status(Integer.parseInt(errorCode)).entity(json).header("Access-Control-Allow-Origin", "*").build();
        } else {
            return Response.noContent().header("Access-Control-Allow-Origin", "*").build();
        }
    }

    @POST
    @Path("token")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Override
    public Response getToken(@HeaderParam("Authorization") String headerAuth, @FormParam("redirect_uri") String redirectUri, @FormParam("grant_type") String grantType, @FormParam("code_verifier") String codeVerifier, @FormParam("code") String code, @FormParam("client_session_state") String clientSessionState) throws Exception {

        Response response;
        ChTokenParamBuilder tokenParam = ChTokenParamBuilder.builder().withGrantType(grantType).withRedirectUri(redirectUri).withCodeVerifier(codeVerifier).withCode(code).build();
        tokenParamValidator.validateTokenRequest(tokenParam);

        try {
            client = new KeycloakClient(ENDPOINT_KEY);
            response = client.requestTokenEndpoint(headerAuth, redirectUri, grantType, codeVerifier, code, clientSessionState);
        } catch (Exception e) {
            return Response.status(Response.Status.NO_CONTENT).header("Access-Control-Allow-Origin", "*").build();
        }
        response.bufferEntity();
        if (response.getStatus() != 200 && response.getStatus() != 202) {
            return response;
        }


        String accessToken = accessTokenResponseValidation.getScopeFromResponse(response);
        ChScopeParamBuilder scopeParam = scopeMapper.getScopeFromQueryParam(accessToken);
        if (scopeParam.getAccessTokenFormat() != null && scopeParam.getAccessTokenFormat().equals("ihe-saml")) {
            try {
                String userSpid = accessTokenResponseValidation.getUserSpidFromResponse(response);
                String assertion = service.generateSAML(scopeParam.getGroup(),
                        scopeParam.getGroupId(), scopeParam.getLaunch(), scopeParam.getPrincipal(),
                        scopeParam.getPersonId(), scopeParam.getPrincipalId(), scopeParam.getPurposeOfUse(), scopeParam.getSubjectRole(), userSpid);
                response = accessTokenResponseValidation.formatSamlResponse(response, assertion);
            } catch (Exception e){
                return Response.status(Response.Status.NO_CONTENT).header("Access-Control-Allow-Origin", "*").entity(new ValidatorErrorMessage("Exception Occurred", false)).build();
            }
        }


        return response;
    }

    @GET
    @Path("health")
    @Override
    public Response getHealth() {
        return Response.ok().header("Access-Control-Allow-Origin", "*").build();
    }

    @GET
    @Path("logout")
    public Response getLogout(){
        try {
            client = new KeycloakClient(ENDPOINT_KEY);
            return client.logoutRequest();
        } catch (Exception e) {
            return Response.status(Response.Status.NO_CONTENT).header("Access-Control-Allow-Origin", "*").build();
        }
    }

}