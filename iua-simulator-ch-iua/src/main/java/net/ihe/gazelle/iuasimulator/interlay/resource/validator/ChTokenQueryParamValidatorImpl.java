package net.ihe.gazelle.iuasimulator.interlay.resource.validator;

import jakarta.inject.Inject;
import net.ihe.gazelle.iuasimulator.application.param.IuaTokenQueryValidator;
import net.ihe.gazelle.iuasimulator.application.utils.RegexUtils;
import net.ihe.gazelle.iuasimulator.domain.exception.ValidatorException;
import net.ihe.gazelle.iuasimulator.domain.model.ChTokenParamBuilder;
import net.ihe.gazelle.iuasimulator.interlay.loggingspe.LogAudited;
import net.ihe.gazelle.iuasimulator.interlay.loggingspe.LogLevel;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.ChTokenParamValidationImpl;

import static java.util.Objects.isNull;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 12/04/2023
 */
@LogAudited(LogLevel.DEBUG)
public class ChTokenQueryParamValidatorImpl implements IuaTokenQueryValidator<ChTokenParamBuilder> {

    private final RegexUtils regexUtils;
    private final ChTokenParamValidationImpl tokenParam;

    @Inject
    public ChTokenQueryParamValidatorImpl(ChTokenParamValidationImpl tokenParam, RegexUtils regexUtils) {
        this.regexUtils = regexUtils;
        this.tokenParam = tokenParam;
    }

    @Override
    public void validateTokenRequest(final ChTokenParamBuilder chTokenParam) {
        if (isNull(chTokenParam.getGrandType()) || chTokenParam.getGrandType().isBlank())
            throw new ValidatorException(tokenParam.getGrantTypeRequired());
        if (isNull(chTokenParam.getCode()) || chTokenParam.getCode().isBlank())
            throw new ValidatorException(tokenParam.getCodeRequired());
        if (isNull(chTokenParam.getCodeVerifier()) || chTokenParam.getCodeVerifier().isBlank())
            throw new ValidatorException(tokenParam.getCodeVerifierRequired());
        if (isNull(chTokenParam.getRedirectUri()) || chTokenParam.getRedirectUri().isBlank())
            throw new ValidatorException(tokenParam.getRedirectUriRequired());

        if (!regexUtils.isValidURL(chTokenParam.getRedirectUri()))
            throw new ValidatorException(tokenParam.getUrlFormat());
    }
}