package net.ihe.gazelle.iuasimulator.interlay.resource.validator;

import jakarta.inject.Inject;
import net.ihe.gazelle.iuasimulator.application.param.IuaAuthQueryValidator;
import net.ihe.gazelle.iuasimulator.application.utils.RegexUtils;
import net.ihe.gazelle.iuasimulator.domain.model.ChAuthParamBuilder;
import net.ihe.gazelle.iuasimulator.domain.exception.ValidatorException;
import net.ihe.gazelle.iuasimulator.interlay.loggingspe.LogAudited;
import net.ihe.gazelle.iuasimulator.interlay.loggingspe.LogLevel;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.ChAuthParamValidationImpl;


import static java.util.Objects.isNull;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 12/04/2023
 */
@LogAudited(LogLevel.DEBUG)
public class ChAuthQueryParamValidatorImpl implements IuaAuthQueryValidator<ChAuthParamBuilder> {

    private final ChAuthParamValidationImpl authParam;
    private final RegexUtils regexUtils;

    @Inject
    public ChAuthQueryParamValidatorImpl(ChAuthParamValidationImpl authParam, RegexUtils regexUtils) {
        this.regexUtils = regexUtils;
        this.authParam = authParam;
    }

    @Override
    public void validateCodeAuthorization(final ChAuthParamBuilder chCodeParam) {
        validateNonEmptyness(chCodeParam);
        validateValues(chCodeParam);
    }

    private void validateValues(ChAuthParamBuilder chCodeParam) {
        if (!regexUtils.isValidURL(chCodeParam.getRedirectUri()))
            throw new ValidatorException(authParam.getUrlFormat());
        if (!regexUtils.isValidURL(chCodeParam.getAudience()))
            throw new ValidatorException(authParam.getUrlFormat());
        if (!regexUtils.isValidCodeChallengeMethodValue(chCodeParam.getCodeChallengeMethod()))
            throw new ValidatorException(authParam.getBadCodeChallengeMethod());
        if (!regexUtils.isValidResponseTypeValue(chCodeParam.getResponseType()))
            throw new ValidatorException(authParam.getBadResponseType());
        if (!regexUtils.isValidCodeChallengeFormat(chCodeParam.getCodeChallenge()))
            throw new ValidatorException(authParam.getBadCodeChallengeFormat());
    }

    private void validateNonEmptyness(ChAuthParamBuilder chCodeParam) {
        first(chCodeParam);
        second(chCodeParam);
    }

    private void second(ChAuthParamBuilder chCodeParam) {
        if (isNull(chCodeParam.getScope()) || chCodeParam.getScope().isBlank())
            throw new ValidatorException(authParam.getScopeRequired());
        if (isNull(chCodeParam.getAudience()) || chCodeParam.getAudience().isBlank())
            throw new ValidatorException(authParam.getAudienceRequired());
        if (isNull(chCodeParam.getCodeChallenge()) || chCodeParam.getCodeChallenge().isBlank())
            throw new ValidatorException(authParam.getCodeChallengeRequired());
        if (isNull(chCodeParam.getCodeChallengeMethod()) || chCodeParam.getCodeChallengeMethod().isBlank())
            throw new ValidatorException(authParam.getCodeChallengeMethodRequired());
    }

    private void first(ChAuthParamBuilder chCodeParam) {
        if (isNull(chCodeParam.getResponseType()) || chCodeParam.getResponseType().isBlank())
            throw new ValidatorException(authParam.getResponseTypeRequired());
        if (isNull(chCodeParam.getClientId()) || chCodeParam.getClientId().isBlank())
            throw new ValidatorException(authParam.getClientIdRequired());
        if (isNull(chCodeParam.getRedirectUri()) || chCodeParam.getRedirectUri().isBlank())
            throw new ValidatorException(authParam.getRedirectUriRequired());
        if (isNull(chCodeParam.getState()) || chCodeParam.getState().isBlank())
            throw new ValidatorException(authParam.getStateRequired());
    }
}