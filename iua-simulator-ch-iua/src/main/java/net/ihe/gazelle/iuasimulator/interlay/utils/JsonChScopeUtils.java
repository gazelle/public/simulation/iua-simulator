package net.ihe.gazelle.iuasimulator.interlay.utils;

import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;

public class JsonChScopeUtils {

    public static final String FALSE = "false";
    private JsonChScopeUtils() { }
    private static final String PROTOCOL_MAPPERS = "protocolMappers";

    public static JsonObjectBuilder createJsonMapper(String value, String name){
        return Json.createObjectBuilder()
                .add("name", value + "mapper")
                .add("protocol", "openid-connect")
                .add("protocolMapper","oidc-hardcoded-claim-mapper")
                .add ("consentRequired", FALSE)
                .add("config", Json.createObjectBuilder()
                        .add("claim.value",value)
                        .add("userinfo.token.claim", FALSE)
                        .add("id.token.claim", FALSE)
                        .add("access.token.claim","true")
                        .add("claim.name","extensions."+name)
                        .add("access.tokenResponse.claim", FALSE));

    }

    public static JsonObjectBuilder createJsonScope(String scope, String id){
        return Json.createObjectBuilder()
                .add("id", id)
                .add("name", scope)
                .add("description", scope + " scope")
                .add("protocol","openid-connect")
                .add("attributes", Json.createObjectBuilder()
                        .add("include.in.token.scope","true")
                        .add("display.on.consent.screen", FALSE)
                        .add("gui.order","")
                        .add("consent.screen.text",""));
    }

    public static JsonObject getAccessTokenFormat(String scope, String value, String id) {
        JsonObject scopeToCreate;
        JsonObjectBuilder groupIdSys = JsonChScopeUtils.createJsonMapper(value,"ch_group.access_token_format");
        JsonObjectBuilder scopeJson = JsonChScopeUtils.createJsonScope(scope +"="+ value, id);
        scopeJson.add(PROTOCOL_MAPPERS, Json.createArrayBuilder().add(groupIdSys));
        scopeToCreate = scopeJson.build();
        return scopeToCreate;
    }

    public static JsonObject getLaunchJson(String scope,String value, String id) {
        JsonObject scopeToCreate;
        JsonObjectBuilder groupIdSys = JsonChScopeUtils.createJsonMapper(value,"ch_iua.launch");
        JsonObjectBuilder scopeJson = JsonChScopeUtils.createJsonScope(scope, id);
        scopeJson.add(PROTOCOL_MAPPERS, Json.createArrayBuilder().add(groupIdSys));
        scopeToCreate = scopeJson.build();
        return scopeToCreate;
    }

    public static JsonObject getGroupIdJson(String scope, String value, String id) {
        JsonObject scopeToCreate;
        JsonObjectBuilder groupIdSys = JsonChScopeUtils.createJsonMapper(value,"ch_group.id");
        JsonObjectBuilder scopeJson = JsonChScopeUtils.createJsonScope(scope +"="+ value, id);
        scopeJson.add(PROTOCOL_MAPPERS, Json.createArrayBuilder().add(groupIdSys));
        scopeToCreate = scopeJson.build();
        return scopeToCreate;
    }

    public static JsonObject getGroupJson(String scope, String value, String id) {
        JsonObject scopeToCreate;
        JsonObjectBuilder groupSys = JsonChScopeUtils.createJsonMapper(value,"ch_group.name");
        JsonObjectBuilder scopeJson = JsonChScopeUtils.createJsonScope(scope +"="+ value, id);
        scopeJson.add(PROTOCOL_MAPPERS, Json.createArrayBuilder().add(groupSys));
        scopeToCreate = scopeJson.build();
        return scopeToCreate;
    }

    public static JsonObject getPrincipalIdJson(String scope, String value, String id) {
        JsonObject scopeToCreate;
        JsonObjectBuilder principalIdSys = JsonChScopeUtils.createJsonMapper(value,"ch_delegation.principal_id");
        JsonObjectBuilder scopeJson = JsonChScopeUtils.createJsonScope(scope +"="+ value, id);
        scopeJson.add(PROTOCOL_MAPPERS, Json.createArrayBuilder().add(principalIdSys));
        scopeToCreate = scopeJson.build();
        return scopeToCreate;
    }

    public static JsonObject getPrincipalJson(String scope, String value, String id) {
        JsonObject scopeToCreate;
        JsonObjectBuilder principalSys = JsonChScopeUtils.createJsonMapper(value,"ch_delegation.principal");
        JsonObjectBuilder scopeJson = JsonChScopeUtils.createJsonScope(scope +"="+ value, id);
        scopeJson.add(PROTOCOL_MAPPERS, Json.createArrayBuilder().add(principalSys));
        scopeToCreate = scopeJson.build();
        return scopeToCreate;
    }

    public static JsonObject getPersonIdJson(String scope, String value, String id) {
        JsonObject scopeToCreate;
        JsonObjectBuilder piSys = JsonChScopeUtils.createJsonMapper(value,"ihe_iua.person_id");
        JsonObjectBuilder scopeJson = JsonChScopeUtils.createJsonScope(scope +"="+ value, id);
        scopeJson.add(PROTOCOL_MAPPERS, Json.createArrayBuilder().add(piSys));
        scopeToCreate = scopeJson.build();
        return scopeToCreate;
    }

    public static JsonObject getSubjectRolejson(String scope, String value, String id) {
        JsonObject scopeToCreate;
        String[] valuesSeparated = value.split("\\|");
        JsonObjectBuilder srSys = JsonChScopeUtils.createJsonMapper(valuesSeparated[0],"ihe_iua.subject_role.system");
        JsonObjectBuilder srCode = JsonChScopeUtils.createJsonMapper(valuesSeparated[1],"ihe_iua.subject_role.code");
        JsonObjectBuilder scopeJson = JsonChScopeUtils.createJsonScope(scope +"="+ value, id);
        scopeJson.add(PROTOCOL_MAPPERS, Json.createArrayBuilder().add(srSys).add(srCode));
        scopeToCreate = scopeJson.build();
        return scopeToCreate;
    }

    public static JsonObject getPurposeOfUseJson(String scope, String value, String id) {
        JsonObject scopeToCreate;
        String[] valuesSeparated = value.split("\\|");
        JsonObjectBuilder pouSys = JsonChScopeUtils.createJsonMapper(valuesSeparated[0],"ihe_iua.purpose_of_use.system");
        JsonObjectBuilder pouCode = JsonChScopeUtils.createJsonMapper(valuesSeparated[1],"ihe_iua.purpose_of_use.code");
        JsonObjectBuilder scopeJson = JsonChScopeUtils.createJsonScope(scope +"="+ value, id);
        scopeJson.add(PROTOCOL_MAPPERS, Json.createArrayBuilder().add(pouSys).add(pouCode));
        scopeToCreate = scopeJson.build();
        return scopeToCreate;
    }
}
