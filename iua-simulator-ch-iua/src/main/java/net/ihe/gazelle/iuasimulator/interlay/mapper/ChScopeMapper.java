package net.ihe.gazelle.iuasimulator.interlay.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.json.JsonObject;
import jakarta.ws.rs.core.Response;
import net.ihe.gazelle.iuasimulator.application.mapper.IuaScopeMapper;
import net.ihe.gazelle.iuasimulator.domain.model.ChScopeParamBuilder;
import net.ihe.gazelle.iuasimulator.interlay.exception.ValidatorErrorMessage;
import net.ihe.gazelle.iuasimulator.interlay.utils.JsonChScopeUtils;
import org.opensaml.xml.util.Pair;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 10/04/2023
 */
public class ChScopeMapper implements IuaScopeMapper<ChScopeParamBuilder> {

    private static final String SCOPE_BLANK = "false";
    public static final String LAUNCH = "launch";

    @Override
    public ChScopeParamBuilder getScopeFromQueryParam(String scope) {
        ChScopeParamBuilder.ChBuilder scopeParam = new ChScopeParamBuilder.ChBuilder();
        for (String s : scope.split(" ")) {
            if (s.contains("=")) {
                String[] scopeValues = s.split("=");
                checkSplitScope(scopeParam, scopeValues);
            } else if (!s.contains("=") && (s.equals(LAUNCH))) {
                scopeParam.withLaunch(s);
            } else {
                String[] scopeValues = new String[]{s};
                checkSplitScope(scopeParam, scopeValues);
            }
            if (!s.contains("=") && (s.equals(LAUNCH))) {
                scopeParam.withLaunch(s);
            }
        }
        return scopeParam.build();
    }

    private static void checkSplitScope(ChScopeParamBuilder.ChBuilder scopeParam, String[] scopeValues) {
        switch (scopeValues[0]) {
            case ("purpose_of_use") ->
                managePurposeOfUse(scopeParam, scopeValues);

            case ("subject_role") ->
                manageSubjectRole(scopeParam, scopeValues);

            case ("person_id") ->
                managePersonId(scopeParam, scopeValues);

            case ("principal") ->
                managePrincipal(scopeParam, scopeValues);

            case ("principal_id") ->
                managePrincipalId(scopeParam, scopeValues);

            case ("group") ->
                manageGroup(scopeParam, scopeValues);

            case ("group_id") ->
                manageGroupId(scopeParam, scopeValues);

            case ("access_token_format") ->
                manageAccessTokenFormat(scopeParam, scopeValues);

            default ->
                    Response.status(Response.Status.BAD_REQUEST).entity(new ValidatorErrorMessage("Unrecognized scope", false)).build();
        }
    }

    private static void manageAccessTokenFormat(ChScopeParamBuilder.ChBuilder scopeParam, String[] scopeValues) {
        if (scopeValues.length == 2) {
            scopeParam.withAccessTokenFormat(scopeValues[1]);
        } else {
            scopeParam.withAccessTokenFormat(SCOPE_BLANK);
        }
    }

    private static void manageGroupId(ChScopeParamBuilder.ChBuilder scopeParam, String[] scopeValues) {
        if (scopeValues.length == 2) {
            scopeParam.withGroupId(scopeValues[1]);
        } else {
            scopeParam.withGroupId(SCOPE_BLANK);
        }
    }

    private static void manageGroup(ChScopeParamBuilder.ChBuilder scopeParam, String[] scopeValues) {
        if (scopeValues.length == 2) {
            scopeParam.withGroup(scopeValues[1]);
        } else {
            scopeParam.withGroup(SCOPE_BLANK);
        }
    }

    private static void managePrincipalId(ChScopeParamBuilder.ChBuilder scopeParam, String[] scopeValues) {
        if (scopeValues.length == 2) {
            scopeParam.withPrincipalId(scopeValues[1]);
        } else {
            scopeParam.withPrincipalId(SCOPE_BLANK);
        }
    }

    private static void managePrincipal(ChScopeParamBuilder.ChBuilder scopeParam, String[] scopeValues) {
        if (scopeValues.length == 2) {
            scopeParam.withPrincipal(scopeValues[1]);
        } else {
            scopeParam.withPrincipal(SCOPE_BLANK);
        }
    }

    private static void managePersonId(ChScopeParamBuilder.ChBuilder scopeParam, String[] scopeValues) {
        if (scopeValues.length == 2) {
            scopeParam.withPersonId(scopeValues[1]);
        } else {
            scopeParam.withPersonId(SCOPE_BLANK);
        }
    }

    private static void manageSubjectRole(ChScopeParamBuilder.ChBuilder scopeParam, String[] scopeValues) {
        if (scopeValues.length == 2) {
            scopeParam.withSubjectRole(scopeValues[1]);
        } else {
            scopeParam.withSubjectRole(SCOPE_BLANK);
        }
    }

    private static void managePurposeOfUse(ChScopeParamBuilder.ChBuilder scopeParam, String[] scopeValues) {
        if (scopeValues.length == 2) {
            scopeParam.withPurposeOfUse(scopeValues[1]);
        } else {
            scopeParam.withPurposeOfUse(SCOPE_BLANK);
        }
    }

    @Override
    public Map<String, String> getScopeMap(ChScopeParamBuilder param) {
        ObjectMapper scopeObject = new ObjectMapper();
        Map<String, String> test = scopeObject.convertValue(param, Map.class);
        Map<String, String> returnedMap = new HashMap<>();
        for (Map.Entry<String, String> entry : test.entrySet()) {
            returnedMap.put(toUnderscore(entry.getKey()), entry.getValue());
        }
        return returnedMap;
    }

    private String toUnderscore(String value) {
        String regex = "([a-z])([A-Z])";
        String replacement = "$1_$2";
        return value.replaceAll(regex, replacement).toLowerCase();
    }

    public Pair<String, JsonObject> scopeClientGeneration(String scope, String value) {
        JsonObject scopeToCreate;
        String id;
        switch (scope) {
            case "purpose_of_use" -> {
                id = "bfc7ed20-8efe-43bd-ae72-72cfa7fd49b9";
                scopeToCreate = JsonChScopeUtils.getPurposeOfUseJson(scope, value, id);
            }
            case "principal" -> {
                id = "bfc7ed20-8efe-43bd-ae72-72cfa7fd4955";
                scopeToCreate = JsonChScopeUtils.getPrincipalJson(scope, value, id);
            }
            case "principal_id" -> {
                id = "bfc7ed20-8efe-43bd-ae72-72cfa7fd4942";
                scopeToCreate = JsonChScopeUtils.getPrincipalIdJson(scope, value, id);
            }
            case "group" -> {
                id = "bfc7ed20-8efe-43bd-ae72-72cfa7fd4901";
                scopeToCreate = JsonChScopeUtils.getGroupJson(scope, value, id);
            }
            case "group_id" -> {
                id = "bfc7ed20-8efe-43bd-ae72-72cfa7fd49ae";
                scopeToCreate = JsonChScopeUtils.getGroupIdJson(scope, value, id);
            }
            case "access_token_format" -> {
                id = "bfc7ed20-8efe-43bd-ae72-72cfa7fu454";
                scopeToCreate = JsonChScopeUtils.getAccessTokenFormat(scope, value, id);
            }
            case "subject_role" -> {
                id = "bfc7ed20-8efe-43bd-ae72-72cfa7fd49a0";
                scopeToCreate = JsonChScopeUtils.getSubjectRolejson(scope, value, id);
            }
            case "person_id" -> {
                id = "bfc7ed20-8efe-43bd-ae72-72cfa7fd49c3";
                scopeToCreate = JsonChScopeUtils.getPersonIdJson(scope, value, id);
            }
            case LAUNCH -> {
                id = "bfc7ed20-8efe-43bd-ae72-72cfa7fd934";
                scopeToCreate = JsonChScopeUtils.getLaunchJson(scope, value, id);
            }
            default -> {
                scopeToCreate = null;
                id = "";
            }
        }
        return new Pair<>(id, scopeToCreate);
    }
}
