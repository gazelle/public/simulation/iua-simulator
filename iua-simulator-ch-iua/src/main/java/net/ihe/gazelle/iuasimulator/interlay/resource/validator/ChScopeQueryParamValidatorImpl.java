package net.ihe.gazelle.iuasimulator.interlay.resource.validator;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import net.ihe.gazelle.iuasimulator.application.param.IuaScopeQueryValidator;
import net.ihe.gazelle.iuasimulator.application.utils.RegexUtils;
import net.ihe.gazelle.iuasimulator.domain.exception.ValidatorException;
import net.ihe.gazelle.iuasimulator.domain.model.ChScopeParamBuilder;
import net.ihe.gazelle.iuasimulator.interlay.loggingspe.LogAudited;
import net.ihe.gazelle.iuasimulator.interlay.loggingspe.LogLevel;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.ChScopeParamValidationImpl;

import java.util.Objects;

import static java.util.Objects.isNull;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 12/04/2023
 */
@LogAudited(LogLevel.DEBUG)
@Singleton
public class ChScopeQueryParamValidatorImpl implements IuaScopeQueryValidator<ChScopeParamBuilder> {

    private static final String CODE_BLANK = "false";
    private final ChScopeParamValidationImpl scopeParam;
    private final RegexUtils regexUtils;


    @Inject
    public ChScopeQueryParamValidatorImpl(ChScopeParamValidationImpl scopeParam, RegexUtils regexUtils) {
        this.scopeParam = scopeParam;
        this.regexUtils = regexUtils;
    }

    @Override
    public void validateScope(ChScopeParamBuilder chScopeParam) {
        validateNonEmptyness(chScopeParam);
        validateValueFormat(chScopeParam);

    }

    private void validateNonEmptyness(ChScopeParamBuilder chScopeParam) {
        if (Objects.equals(chScopeParam.getPurposeOfUse(), CODE_BLANK))
            throw new ValidatorException(scopeParam.getPurposeOfUseRequired());
        if (Objects.equals(chScopeParam.getSubjectRole(), CODE_BLANK))
            throw new ValidatorException(scopeParam.getSubjectRoleRequired());
        if (Objects.equals(chScopeParam.getPrincipal(), CODE_BLANK))
            throw new ValidatorException(scopeParam.getPrincipalRequired());
        if (Objects.equals(chScopeParam.getPrincipalId(), CODE_BLANK))
            throw new ValidatorException(scopeParam.getPrincipalIdRequired());
        if (Objects.equals(chScopeParam.getPersonId(), CODE_BLANK))
            throw new ValidatorException(scopeParam.getPersonIdRequired());
        if (Objects.equals(chScopeParam.getGroup(), CODE_BLANK))
            throw new ValidatorException(scopeParam.getGroupRequired());
        if (Objects.equals(chScopeParam.getGroupId(), CODE_BLANK))
            throw new ValidatorException(scopeParam.getGroupIdRequired());
        if (Objects.equals(chScopeParam.getAccessTokenFormat(), CODE_BLANK))
            throw new ValidatorException(scopeParam.getAccessTokenFormatRequired());
    }

    private void validateValueFormat(ChScopeParamBuilder chScopeParam) {
        if (!isNull(chScopeParam.getAccessTokenFormat()) && !regexUtils.isValidAccessTokenFormat(chScopeParam.getAccessTokenFormat()))
            throw new ValidatorException(scopeParam.getAccessTokenFormat());
        if (!isNull(chScopeParam.getSubjectRole()) && !regexUtils.isValidSubjectRoleFormat(chScopeParam.getSubjectRole()))
            throw new ValidatorException(scopeParam.getSubjectRoleFormat());
        if (!isNull(chScopeParam.getPurposeOfUse()) && !regexUtils.isValidPurposeOfUseFormat(chScopeParam.getPurposeOfUse()))
            throw new ValidatorException(scopeParam.getPurposeOfUseFormat());
        if (!isNull(chScopeParam.getGroupId()) && !regexUtils.isValidGroupIdFormat(chScopeParam.getGroupId()))
            throw new ValidatorException(scopeParam.getGroupIdFormat());
        if (!isNull(chScopeParam.getPersonId()) && !regexUtils.isValidPersonIdFormat(chScopeParam.getPersonId()))
            throw new ValidatorException(scopeParam.getPersonIdFormat());
        if (!isNull(chScopeParam.getPrincipalId()) && !regexUtils.isValidPrincipalIdFormat(chScopeParam.getPrincipalId()))
            throw new ValidatorException(scopeParam.getPrincipalIdFormat());
    }
}