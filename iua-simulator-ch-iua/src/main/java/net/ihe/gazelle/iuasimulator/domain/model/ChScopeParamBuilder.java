package net.ihe.gazelle.iuasimulator.domain.model;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 09/04/2023
 */
public class ChScopeParamBuilder extends IuaScopeParamBuilder {

    private final String purposeOfUse;
    private final String subjectRole;
    private final String personId;
    private final String principal;
    private final String principalId;
    private final String group;
    private final String groupId;
    private final String accessTokenFormat;
    private final String launch;

    public ChScopeParamBuilder(ChBuilder builder) {
        super(builder);
        this.launch = builder.launch;
        this.purposeOfUse = builder.purposeOfUse;
        this.subjectRole = builder.subjectRole;
        this.personId = builder.personId;
        this.principal = builder.principal;
        this.principalId = builder.principalId;
        this.group = builder.group;
        this.groupId = builder.groupId;
        this.accessTokenFormat = builder.accessTokenFormat;
    }

    public static ChBuilder builder() {
        return new ChBuilder();
    }

    public static class ChBuilder extends IuaBuilder<ChBuilder> {
        private String purposeOfUse;
        private String subjectRole;
        private String personId;
        private String principal;
        private String principalId;
        private String group;
        private String groupId;
        private String accessTokenFormat;
        private String launch;
        @Override
        public ChBuilder getThis() {
            return this;
        }

        public ChBuilder withPurposeOfUse(String purposeOfUse) {
            this.purposeOfUse = purposeOfUse;
            return this.getThis();
        }

        public ChBuilder withSubjectRole(String subjectRole) {
            this.subjectRole = subjectRole;
            return this.getThis();
        }

        public ChBuilder withPersonId(String personId) {
            this.personId = personId;
            return this.getThis();
        }

        public ChBuilder withPrincipal(String principal) {
            this.principal = principal;
            return this.getThis();
        }

        public ChBuilder withPrincipalId(String principalId) {
            this.principalId = principalId;
            return this.getThis();
        }

        public ChBuilder withGroup(String group) {
            this.group = group;
            return this.getThis();
        }

        public ChBuilder withGroupId(String groupId) {
            this.groupId = groupId;
            return this.getThis();
        }

        public ChBuilder withAccessTokenFormat(String accessTokenFormat) {
            this.accessTokenFormat = accessTokenFormat;
            return this.getThis();
        }

        public ChBuilder withLaunch(String launch) {
            this.launch = launch;
            return this;
        }

        @Override
        public ChScopeParamBuilder build() {
            return new ChScopeParamBuilder(this);
        }
    }

    public String getPurposeOfUse() {
        return purposeOfUse;
    }
    public String getSubjectRole() {
        return subjectRole;
    }
    public String getPersonId() {
        return personId;
    }
    public String getPrincipal() {
        return principal;
    }
    public String getPrincipalId() {
        return principalId;
    }
    public String getGroup() {
        return group;
    }
    public String getGroupId() {
        return groupId;
    }
    public String getAccessTokenFormat() {
        return accessTokenFormat;
    }
    public String getLaunch() {
        return launch;
    }
}
