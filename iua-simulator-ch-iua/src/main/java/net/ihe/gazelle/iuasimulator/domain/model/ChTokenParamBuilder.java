package net.ihe.gazelle.iuasimulator.domain.model;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 09/04/2023
 */
public class ChTokenParamBuilder extends IuaTokenParamBuilder {

    protected ChTokenParamBuilder(ChBuilder builder) {
        super(builder);
    }

    public static ChBuilder builder() {
        return new ChBuilder();
    }

    public static class ChBuilder extends IuaBuilder<ChBuilder> {

        @Override
        public ChBuilder getThis() {
            return this;
        }

        @Override
        public ChTokenParamBuilder build() { return new ChTokenParamBuilder(this);}
    }
}