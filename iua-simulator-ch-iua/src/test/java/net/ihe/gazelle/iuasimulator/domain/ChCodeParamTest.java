package net.ihe.gazelle.iuasimulator.domain;

import net.ihe.gazelle.iuasimulator.domain.model.ChAuthParamBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 11/04/2023
 */
class ChCodeParamTest {
        @Test
        void getChCodeParamTest_ok() {
            ChAuthParamBuilder chCodeParam = ChAuthParamBuilder.builder()
                    .withResponseType("responseType")
                    .withClientId("clientId")
                    .withRedirectUri("redirectUri")
                    .withResource("resource")
                    .withState("state")
                    .withScope("scope")
                    .withAudience("audience")
                    .withCodeChallenge("codeChallenge")
                    .withCodeChallengeMethod("S256")
                    .withLaunch("launch").build();
            Assertions.assertEquals("responseType", chCodeParam.getResponseType());
            Assertions.assertEquals("clientId", chCodeParam.getClientId());
            Assertions.assertEquals("redirectUri", chCodeParam.getRedirectUri());
            Assertions.assertEquals("resource", chCodeParam.getResource());
            Assertions.assertEquals("state", chCodeParam.getState());
            Assertions.assertEquals("scope", chCodeParam.getScope());
            Assertions.assertEquals("audience", chCodeParam.getAudience());
            Assertions.assertEquals("codeChallenge", chCodeParam.getCodeChallenge());
            Assertions.assertEquals("S256", chCodeParam.getCodeChallengeMethod());
            Assertions.assertEquals("launch", chCodeParam.getLaunch());
        }
}
