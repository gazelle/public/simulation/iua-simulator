package net.ihe.gazelle.iuasimulator.interlay.resource.validator;

import net.ihe.gazelle.iuasimulator.application.param.IuaScopeQueryValidator;
import net.ihe.gazelle.iuasimulator.application.utils.RegexUtils;
import net.ihe.gazelle.iuasimulator.domain.exception.ValidatorException;
import net.ihe.gazelle.iuasimulator.domain.model.ChScopeParamBuilder;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.ChScopeParamValidationImpl;
import net.ihe.gazelle.iuasimulator.interlay.utils.RegexUtilsImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 14/04/2023
 */
class ChScopeQueryParamValidatorImplTest {

    private final ChScopeParamValidationImpl scopeParam = new ChScopeParamValidationImpl();
    private final RegexUtils regexUtils = new RegexUtilsImpl();
    private final IuaScopeQueryValidator<ChScopeParamBuilder> validator = new ChScopeQueryParamValidatorImpl(scopeParam, regexUtils);

    @Test
    void validateScope_ko_purposeOfUse() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withPurposeOfUse("urn:oid:2.16.756.5.30.1.127.3.10.5").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

    @Test
    void validateScope_ko_accessTokenFormat() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withAccessTokenFormat("ihe_jwt").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

    @Test
    void validateScope_ko_groupId() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withGroupId("null").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

    @Test
    void validateScope_ko_principalId() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withPrincipalId("fdifejfos").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

    @Test
    void validateScope_ko_personId() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withPersonId("").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

    @Test
    void validateScope_ko_subjectRole() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withSubjectRole("").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

    @Test
    void validateScope_ko_purposeOfUse_bland() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withPurposeOfUse("false").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

    @Test
    void validateScope_ko_accessTokenFormat_bland() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withAccessTokenFormat("false").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

    @Test
    void validateScope_ko_groupId_bland() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withGroupId("false").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

    @Test
    void validateScope_ko_principalId_bland() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withPrincipalId("false").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

    @Test
    void validateScope_ko_personId_bland() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withPersonId("false").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

    @Test
    void validateScope_ko_subjectRole_bland() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withSubjectRole("false").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

    @Test
    void validateScope_ko_principal_bland() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withPrincipal("false").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

    @Test
    void validateScope_ko_group_bland() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withGroup("false").build();
        assertThrows(ValidatorException.class, () -> validator.validateScope(scopeParam));
    }

}