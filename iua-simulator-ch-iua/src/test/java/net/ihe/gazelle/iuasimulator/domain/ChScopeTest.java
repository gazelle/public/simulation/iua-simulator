package net.ihe.gazelle.iuasimulator.domain;

import net.ihe.gazelle.iuasimulator.domain.model.ChScopeParamBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 11/04/2023
 */
class ChScopeTest {

    @Test
    void getChScopeParamTest_ok() {
        ChScopeParamBuilder scopeParam = ChScopeParamBuilder.builder()
                .withPurposeOfUse("purposeOfUse")
                .withSubjectRole("subjectRole")
                .withPersonId("personId")
                .withPrincipal("principal")
                .withPrincipalId("principalId")
                .withGroup("group")
                .withGroupId("groupId")
                .withAccessTokenFormat("accessTokenFormat")
                .withLaunch("false").build();
        Assertions.assertEquals("purposeOfUse", scopeParam.getPurposeOfUse());
        Assertions.assertEquals("subjectRole", scopeParam.getSubjectRole());
        Assertions.assertEquals("personId", scopeParam.getPersonId());
        Assertions.assertEquals("principal", scopeParam.getPrincipal());
        Assertions.assertEquals("principalId", scopeParam.getPrincipalId());
        Assertions.assertEquals("group", scopeParam.getGroup());
        Assertions.assertEquals("groupId", scopeParam.getGroupId());
        Assertions.assertEquals("accessTokenFormat", scopeParam.getAccessTokenFormat());
        Assertions.assertEquals("false", scopeParam.getLaunch());
    }
}
