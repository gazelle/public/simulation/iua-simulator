package net.ihe.gazelle.iuasimulator.interlay.mapper;

import jakarta.json.JsonObject;
import net.ihe.gazelle.iuasimulator.domain.model.ChScopeParamBuilder;
import net.ihe.gazelle.iuasimulator.interlay.utils.JsonChScopeUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.opensaml.xml.util.Pair;

import java.util.Map;
import java.util.Objects;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 14/04/2023
 */
class ChScopeMapperTest {

    private final ChScopeMapper mapper = new ChScopeMapper();

    @Test
    void getScopeFromQueryParamTest_ok() {
        String scope = "launch purpose_of_use=urn:oid:2.16.756.5.30.1.127.3.10.5|NORM principal=test principal_id=test group=test group_id=test access_token_format=JWT subject_role=urn:oid:2.16.756.5.30.1.127.3.10.6|HCP person_id=761337610411353650%5E%5E%5E%26amp%3B2.16.756.5.30.1.127.3.10.3%26amp%3BISO";
        ChScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scope);
        Assertions.assertEquals("urn:oid:2.16.756.5.30.1.127.3.10.5|NORM", scopeParam.getPurposeOfUse());
        Assertions.assertEquals("urn:oid:2.16.756.5.30.1.127.3.10.6|HCP", scopeParam.getSubjectRole());
        Assertions.assertEquals("JWT", scopeParam.getAccessTokenFormat());
        Assertions.assertEquals("test", scopeParam.getGroup());
        Assertions.assertEquals("test", scopeParam.getGroupId());
        Assertions.assertEquals("test", scopeParam.getPrincipal());
        Assertions.assertEquals("launch", scopeParam.getLaunch());
        Assertions.assertEquals("761337610411353650%5E%5E%5E%26amp%3B2.16.756.5.30.1.127.3.10.3%26amp%3BISO", scopeParam.getPersonId());
    }

    @Test
    void getScopeFromQueryParamTest_false_values() {
        String scope = "launch purpose_of_use principal principal_id group= group_id= access_token_format= subject_role person_id=";
        ChScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scope);
        Assertions.assertEquals("false", scopeParam.getPurposeOfUse());
        Assertions.assertEquals("false", scopeParam.getSubjectRole());
        Assertions.assertEquals("false", scopeParam.getAccessTokenFormat());
        Assertions.assertEquals("false", scopeParam.getGroup());
        Assertions.assertEquals("false", scopeParam.getGroupId());
        Assertions.assertEquals("false", scopeParam.getPrincipal());
        Assertions.assertEquals("launch", scopeParam.getLaunch());
        Assertions.assertEquals("false", scopeParam.getPersonId());
    }

    @Test
    void getScopeFromQueryParamTest_ko() {
        String scope = "";
        ChScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scope);
        Assertions.assertFalse(scopeParam.equals(200));
    }

    @Test
    void getScopeMap_ok() {
        String scope = "launch purpose_of_use=urn:oid:2.16.756.5.30.1.127.3.10.5|NORM principal=test principal_id=test group=test group_id=test access_token_format=JWT subject_role=urn:oid:2.16.756.5.30.1.127.3.10.6|HCP person_id=761337610411353650%5E%5E%5E%26%3B2.16.756.5.30.1.127.3.10.3%26%3BISO";
        ChScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scope);
        Map<String, String> map = mapper.getScopeMap(scopeParam);
        Assertions.assertFalse(map.isEmpty());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (Objects.equals(entry.getKey(), "principal")) {
                Assertions.assertEquals("test", entry.getValue());
            }
            if (Objects.equals(entry.getKey(), "purpose_of_use")) {
                Assertions.assertEquals("urn:oid:2.16.756.5.30.1.127.3.10.5|NORM", entry.getValue());
            }

        }
    }

    @Test
    void scopeClientGenerationPouTest() {
        String scope = "purpose_of_use=urn:oid:2.16.756.5.30.1.127.3.10.5|NORM";
        String idToBeUsed = "bfc7ed20-8efe-43bd-ae72-72cfa7fd49b9";
        JsonObject jsonToBeGenerated = JsonChScopeUtils.getPurposeOfUseJson("purpose_of_use","urn:oid:2.16.756.5.30.1.127.3.10.5|NORM",idToBeUsed);
        ChScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scope);
        Map<String, String> scopeMap = mapper.getScopeMap(scopeParam);

        for (Map.Entry<String, String> entry : scopeMap.entrySet()) {
            if (entry.getValue() != null) {
                Pair<String, JsonObject> scopeToCreate = mapper.scopeClientGeneration(entry.getKey(), entry.getValue());
                Assertions.assertEquals(idToBeUsed, scopeToCreate.getFirst());
                Assertions.assertEquals(jsonToBeGenerated, scopeToCreate.getSecond());
            }
        }
    }

    @Test
    void scopeClientGenerationPrincipalTest() {
        String scope = "principal=test";
        String idToBeUsed = "bfc7ed20-8efe-43bd-ae72-72cfa7fd4955";
        JsonObject jsonToBeGenerated = JsonChScopeUtils.getPrincipalJson("principal","test",idToBeUsed);
        ChScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scope);
        Map<String, String> scopeMap = mapper.getScopeMap(scopeParam);

        for (Map.Entry<String, String> entry : scopeMap.entrySet()) {
            if (entry.getValue() != null) {
                Pair<String, JsonObject> scopeToCreate = mapper.scopeClientGeneration(entry.getKey(), entry.getValue());
                Assertions.assertEquals(idToBeUsed, scopeToCreate.getFirst());
                Assertions.assertEquals(jsonToBeGenerated, scopeToCreate.getSecond());
            }
        }
    }

    @Test
    void scopeClientGenerationPrincipalIdTest() {
        String scope = "principal_id=test";
        String idToBeUsed = "bfc7ed20-8efe-43bd-ae72-72cfa7fd4942";
        JsonObject jsonToBeGenerated = JsonChScopeUtils.getPrincipalIdJson("principal_id","test",idToBeUsed);
        ChScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scope);
        Map<String, String> scopeMap = mapper.getScopeMap(scopeParam);

        for (Map.Entry<String, String> entry : scopeMap.entrySet()) {
            if (entry.getValue() != null) {
                Pair<String, JsonObject> scopeToCreate = mapper.scopeClientGeneration(entry.getKey(), entry.getValue());
                Assertions.assertEquals(idToBeUsed, scopeToCreate.getFirst());
                Assertions.assertEquals(jsonToBeGenerated, scopeToCreate.getSecond());
            }
        }
    }

    @Test
    void scopeClientGenerationGroupTest() {
        String scope = "group=test";
        String idToBeUsed = "bfc7ed20-8efe-43bd-ae72-72cfa7fd4901";
        JsonObject jsonToBeGenerated = JsonChScopeUtils.getGroupJson("group","test",idToBeUsed);
        ChScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scope);
        Map<String, String> scopeMap = mapper.getScopeMap(scopeParam);

        for (Map.Entry<String, String> entry : scopeMap.entrySet()) {
            if (entry.getValue() != null) {
                Pair<String, JsonObject> scopeToCreate = mapper.scopeClientGeneration(entry.getKey(), entry.getValue());
                Assertions.assertEquals(idToBeUsed, scopeToCreate.getFirst());
                Assertions.assertEquals(jsonToBeGenerated, scopeToCreate.getSecond());
            }
        }
    }

    @Test
    void scopeClientGenerationGroupIdTest() {
        String scope = "group_id=test";
        String idToBeUsed = "bfc7ed20-8efe-43bd-ae72-72cfa7fd49ae";
        JsonObject jsonToBeGenerated = JsonChScopeUtils.getGroupIdJson("group_id","test",idToBeUsed);
        ChScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scope);
        Map<String, String> scopeMap = mapper.getScopeMap(scopeParam);

        for (Map.Entry<String, String> entry : scopeMap.entrySet()) {
            if (entry.getValue() != null) {
                Pair<String, JsonObject> scopeToCreate = mapper.scopeClientGeneration(entry.getKey(), entry.getValue());
                Assertions.assertEquals(idToBeUsed, scopeToCreate.getFirst());
                Assertions.assertEquals(jsonToBeGenerated, scopeToCreate.getSecond());
            }
        }
    }

    @Test
    void scopeClientGenerationAccessTokenFormatTest() {
        String scope = "access_token_format=JWT";
        String idToBeUsed = "bfc7ed20-8efe-43bd-ae72-72cfa7fu454";
        JsonObject jsonToBeGenerated = JsonChScopeUtils.getAccessTokenFormat("access_token_format","JWT",idToBeUsed);
        ChScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scope);
        Map<String, String> scopeMap = mapper.getScopeMap(scopeParam);

        for (Map.Entry<String, String> entry : scopeMap.entrySet()) {
            if (entry.getValue() != null) {
                Pair<String, JsonObject> scopeToCreate = mapper.scopeClientGeneration(entry.getKey(), entry.getValue());
                Assertions.assertEquals(idToBeUsed, scopeToCreate.getFirst());
                Assertions.assertEquals(jsonToBeGenerated, scopeToCreate.getSecond());
            }
        }
    }

    @Test
    void scopeClientGenerationSubjectRoleTest() {
        String scope = "subject_role=urn:oid:2.16.756.5.30.1.127.3.10.6|HCP";
        String idToBeUsed = "bfc7ed20-8efe-43bd-ae72-72cfa7fd49a0";
        JsonObject jsonToBeGenerated = JsonChScopeUtils.getSubjectRolejson("subject_role","urn:oid:2.16.756.5.30.1.127.3.10.6|HCP",idToBeUsed);
        ChScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scope);
        Map<String, String> scopeMap = mapper.getScopeMap(scopeParam);

        for (Map.Entry<String, String> entry : scopeMap.entrySet()) {
            if (entry.getValue() != null) {
                Pair<String, JsonObject> scopeToCreate = mapper.scopeClientGeneration(entry.getKey(), entry.getValue());
                Assertions.assertEquals(idToBeUsed, scopeToCreate.getFirst());
                Assertions.assertEquals(jsonToBeGenerated, scopeToCreate.getSecond());
            }
        }
    }

    @Test
    void scopeClientGenerationPersonIdTest() {
        String scope = "person_id=761337610411353650%5E%5E%5E%26amp%3B2.16.756.5.30.1.127.3.10.3%26amp%3BISO";
        String idToBeUsed = "bfc7ed20-8efe-43bd-ae72-72cfa7fd49c3";
        JsonObject jsonToBeGenerated = JsonChScopeUtils.getPersonIdJson("person_id","761337610411353650%5E%5E%5E%26amp%3B2.16.756.5.30.1.127.3.10.3%26amp%3BISO",idToBeUsed);
        ChScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scope);
        Map<String, String> scopeMap = mapper.getScopeMap(scopeParam);

        for (Map.Entry<String, String> entry : scopeMap.entrySet()) {
            if (entry.getValue() != null) {
                Pair<String, JsonObject> scopeToCreate = mapper.scopeClientGeneration(entry.getKey(), entry.getValue());
                Assertions.assertEquals(idToBeUsed, scopeToCreate.getFirst());
                Assertions.assertEquals(jsonToBeGenerated, scopeToCreate.getSecond());
            }
        }
    }
    @Test
    void scopeClientGenerationLaunchTest() {
        String scope = "launch";
        String idToBeUsed = "bfc7ed20-8efe-43bd-ae72-72cfa7fd934";
        JsonObject jsonToBeGenerated = JsonChScopeUtils.getLaunchJson("launch","launch",idToBeUsed);
        ChScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scope);
        Map<String, String> scopeMap = mapper.getScopeMap(scopeParam);

        for (Map.Entry<String, String> entry : scopeMap.entrySet()) {
            if (entry.getValue() != null) {
                Pair<String, JsonObject> scopeToCreate = mapper.scopeClientGeneration(entry.getKey(), entry.getValue());
                Assertions.assertEquals(idToBeUsed, scopeToCreate.getFirst());
                Assertions.assertEquals(jsonToBeGenerated, scopeToCreate.getSecond());
            }
        }
    }

    @Test
    void scopeClientGenerationFailTest() {
        String scope = "gloubiboulga";
        String idToBeUsed = "";
        Pair<String, JsonObject> scopeToCreate = mapper.scopeClientGeneration(scope, scope);
        Assertions.assertEquals(idToBeUsed, scopeToCreate.getFirst());
        Assertions.assertNull(scopeToCreate.getSecond());
    }
}