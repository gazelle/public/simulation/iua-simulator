package net.ihe.gazelle.iuasimulator.interlay.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 15/04/2023
 */
class RegexUtilsImplTest {

    private final RegexUtilsImpl utils = new RegexUtilsImpl();

    @Test
    void isValidPersonIdFormat() {
        boolean test = utils.isValidPersonIdFormat("761337610411353650^^^&;2.16.756.5.30.1.127.3.10.3&;ISO");
        assertTrue(test);
    }

    @Test
    void isValidPersonIdFormat_ko() {
        boolean test = utils.isValidPersonIdFormat("761337610411353652.16.756.5.30.1.127.3.10.3&amp;ISO");
        assertFalse(test);
    }

    @Test
    void isValidPurposeOfUseFormat() {
        boolean test = utils.isValidPurposeOfUseFormat("urn:oid:2.16.756.5.30.1.127.3.10.5|NORM");
        assertTrue(test);
    }

    @Test
    void isValidPurposeOfUseFormat_ko() {
        boolean test = utils.isValidPurposeOfUseFormat("urn:oid:2.16.756.5.30.1.127.3.10.5|");
        assertFalse(test);
    }

    @Test
    void isValidSubjectRoleFormat() {
        boolean test = utils.isValidSubjectRoleFormat("urn:oid:2.16.756.5.30.1.127.3.10.6|ASS");
        assertTrue(test);
    }

    @Test
    void isValidSubjectRoleFormat_ko() {
        boolean test = utils.isValidSubjectRoleFormat("urn:oid:2.16.756.5.30.1.127.3.10.6|");
        assertFalse(test);
    }

    @Test
    void isValidGroupIdFormat() {
        boolean test = utils.isValidGroupIdFormat( "urn:oid:2.1.7.5");
        assertTrue(test);
    }

    @Test
    void isValidGroupIdFormat_ko() {
        boolean test = utils.isValidGroupIdFormat("urn:oid:2.16.756.5");
        assertFalse(test);
    }

    @Test
    void isValidPrincipalIdFormat() {
        boolean test = utils.isValidPrincipalIdFormat("1928374837291");
        assertTrue(test);
    }

    @Test
    void isValidPrincipalIdFormat_ko() {
        boolean test = utils.isValidPrincipalIdFormat("291829304921");
        assertFalse(test);
    }

    @Test
    void isValidAccessTokenFormat() {
        boolean test = utils.isValidAccessTokenFormat("ihe-jwt");
        assertTrue(test);
    }

    @Test
    void isValidAccessTokenFormat_ko() {
        boolean test = utils.isValidAccessTokenFormat("ihe");
        assertFalse(test);
    }

    @Test
    void isValidResponseTypeValue() {
        boolean test = utils.isValidResponseTypeValue("code");
        assertTrue(test);
    }

    @Test
    void isValidResponseTypeValue_ko() {
        boolean test = utils.isValidResponseTypeValue("pouet");
        assertFalse(test);
    }

    @Test
    void isValidCodeChallengeMethodValue() {
        boolean test = utils.isValidCodeChallengeMethodValue("S256");
        assertTrue(test);
    }

    @Test
    void isValidCodeChallengeMethodValue_ko() {
        boolean test = utils.isValidCodeChallengeMethodValue("SH256");
        assertFalse(test);
    }

    @Test
    void isValidCodeChallengeFormat() {
        boolean test = utils.isValidCodeChallengeFormat("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg");
        assertTrue(test);
    }

    @Test
    void isValidCodeChallengeFormat_ko() {
        boolean test = utils.isValidCodeChallengeFormat("fDH8WRQ-vp-6dUFa7wHYSoiKOWoDNxl_u0TLg");
        assertFalse(test);
    }

    @Test
    void isValidUrlFormat_ok() {
        boolean test = utils.isValidURL("http://localhost:8080/test");
        assertTrue(test);
    }
    @Test
    void isValidUrlFormat_ko() {
        boolean test = utils.isValidURL("http:/localhost:8080/test");
        assertFalse(test);
    }
}