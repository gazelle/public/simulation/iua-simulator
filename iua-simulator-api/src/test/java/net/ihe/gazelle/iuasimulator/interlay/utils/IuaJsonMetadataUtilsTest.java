package net.ihe.gazelle.iuasimulator.interlay.utils;


import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.json.JsonObject;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class IuaJsonMetadataUtilsTest {

    private final IuaJsonMetadataUtils utils = new IuaJsonMetadataUtils("ihe");

    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    void generateMetadataTest(){
        JsonObject jsonMetadata = utils.generateMetadata("localhost:8080");

        String metadata = jsonMetadata.toString();
        JsonNode tokenNode;
        try {
            tokenNode = mapper.readTree(metadata);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        assertNotNull(tokenNode.get("issuer").asText());
        assertNotNull(tokenNode.get("authorization_endpoint").asText());
        assertNotNull(tokenNode.get("token_endpoint").asText());
        assertNotNull(tokenNode.get("jwks_uri").asText());
        assertNotNull(tokenNode.get("scopes").asText());
        assertNotNull(tokenNode.get("response_types_supported").asText());
        assertNotNull(tokenNode.get("grant_types_supported").asText());
        assertNotNull(tokenNode.get("introspection_endpoint").asText());
        assertNotNull(tokenNode.get("access_token_format").asText());
    }
}
