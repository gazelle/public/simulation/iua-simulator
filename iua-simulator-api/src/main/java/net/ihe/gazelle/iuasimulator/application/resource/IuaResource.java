package net.ihe.gazelle.iuasimulator.application.resource;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 03/02/2023
 */

public interface IuaResource<T> {

    T getAuthCode(String responseType, String clientId, String state, String resource, String codeChallenge, String codeChallengeMethod, String redirectUri, String scope, String audience, String launch) throws UnsupportedEncodingException, NoSuchAlgorithmException;

    T getMetadata();

    T retrieveCode(String state, String sessionState, String code, String errorCode, String message);

    T getToken(String headerAuth, String redirectUri, String grantType, String codeVerifier, String code, String clientSessionState) throws Exception;

    T getHealth();
}
