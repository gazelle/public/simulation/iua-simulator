package net.ihe.gazelle.iuasimulator.domain.model;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 07/04/2023
 */
public class IuaScopeParamBuilder {


    protected IuaScopeParamBuilder(IuaBuilder<?> builder) {
    }

    public static IuaBuilder builder() {
        return new IuaBuilder() {
            @Override
            public IuaBuilder getThis() {
                return this;
            }
        };
    }


    public abstract static class IuaBuilder<T extends IuaBuilder<T>> {

        public abstract T getThis();

        public IuaScopeParamBuilder build() {
            return new IuaScopeParamBuilder(this);
        }
    }

}
