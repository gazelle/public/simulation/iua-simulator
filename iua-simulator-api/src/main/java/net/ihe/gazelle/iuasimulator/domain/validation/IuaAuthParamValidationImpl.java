package net.ihe.gazelle.iuasimulator.domain.validation;


import net.ihe.gazelle.iuasimulator.application.validation.IuaAuthParamValidation;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 17/04/2023
 */
public abstract class IuaAuthParamValidationImpl implements IuaAuthParamValidation {

    private String urlFormat;
    private String redirectUriValue;
    private String badClientName;
    private String responseTypeRequired;
    private String clientIdRequired;
    private String stateRequired;
    private String codeChallengeRequired;
    private String redirectUriRequired;
    private String codeChallengeMethodRequired;
    private String badResponseType;
    private String badCodeChallengeMethod;
    private String badCodeChallengeFormat;

    @Override
    public String getBadCodeChallengeFormat() {
        return this.badCodeChallengeFormat;
    }

    @Override
    public String getBadCodeChallengeMethod() {
        return this.badCodeChallengeMethod;
    }

    @Override
    public String getBadResponseType() {
        return this.badResponseType;
    }

    @Override
    public String getUrlFormat() {
        return this.urlFormat;
    }

    @Override
    public String getRedirectUriValue() {
        return this.redirectUriValue;
    }

    @Override
    public String getBadClientName() {
        return this.badClientName;
    }

    @Override
    public String getResponseTypeRequired() {
        return this.responseTypeRequired;
    }

    @Override
    public String getClientIdRequired() {
        return this.clientIdRequired;
    }

    @Override
    public String getStateRequired() {
        return this.stateRequired;
    }

    @Override
    public String getCodeChallengeRequired() {
        return this.codeChallengeRequired;
    }

    @Override
    public String getRedirectUriRequired() {
        return this.redirectUriRequired;
    }

    @Override
    public String getCodeChallengeMethodRequired() {
        return this.codeChallengeMethodRequired;
    }
}
