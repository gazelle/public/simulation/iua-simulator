package net.ihe.gazelle.iuasimulator.domain.exception;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @project site_perso
 * @package fr.lusseau.infrastructure.resource.exception
 * @date 22/08/2022
 */
public class ValidatorErrorMessage {

    private String message;
    private Boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

	public ValidatorErrorMessage(String message, Boolean status) {
		super();
		this.message = message;
		this.status = status;
	}

	public ValidatorErrorMessage() {
		super();
	}

}