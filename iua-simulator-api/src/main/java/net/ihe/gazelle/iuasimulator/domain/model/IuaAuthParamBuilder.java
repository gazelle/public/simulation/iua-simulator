package net.ihe.gazelle.iuasimulator.domain.model;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 06/04/2023
 */
public class IuaAuthParamBuilder {

    private final String responseType;
    private final String clientId;
    private final String redirectUri;
    private final String resource;
    private final String state;
    private final String scope;
    private final String audience;
    private final String codeChallenge;
    private final String codeChallengeMethod;

    protected IuaAuthParamBuilder(IuaBuilder<?> iuaBuilder) {
        this.responseType = iuaBuilder.responseType;
        this.clientId = iuaBuilder.clientId;
        this.redirectUri = iuaBuilder.redirectUri;
        this.resource = iuaBuilder.resource;
        this.state = iuaBuilder.state;
        this.scope = iuaBuilder.scope;
        this.audience = iuaBuilder.audience;
        this.codeChallenge = iuaBuilder.codeChallenge;
        this.codeChallengeMethod = iuaBuilder.codeChallengeMethod;
    }

    public static IuaBuilder builder() {
        return new IuaBuilder() {
            @Override
            public IuaBuilder getThis() {
                return this;
            }
        };
    }

    public abstract static class IuaBuilder<T extends IuaBuilder<T>> {

        private String responseType;
        private String clientId;
        private String redirectUri;
        private String resource;
        private String state;
        private String scope;
        private String audience;
        private String codeChallenge;
        private String codeChallengeMethod;

        public abstract T getThis();

        public T withResponseType(String responseType) {
            this.responseType = responseType;
            return this.getThis();
        }
        public T withClientId(String clientId) {
            this.clientId = clientId;
            return this.getThis();
        }
        public T withRedirectUri(String redirectUri) {
            this.redirectUri = redirectUri;
            return this.getThis();
        }
        public T withResource(String resource) {
            this.resource = resource;
            return this.getThis();
        }
        public T withState(String state) {
            this.state = state;
            return this.getThis();
        }
        public T withScope(String scope) {
            this.scope = scope;
            return this.getThis();
        }
        public T withAudience(String audience) {
            this.audience = audience;
            return this.getThis();
        }

        public T withCodeChallenge(String codeChallenge) {
            this.codeChallenge = codeChallenge;
            return this.getThis();
        }

        public T withCodeChallengeMethod(String codeChallengeMethod) {
            this.codeChallengeMethod = codeChallengeMethod;
            return this.getThis();
        }


        public IuaAuthParamBuilder build() {
            return new IuaAuthParamBuilder(this);
        }
    }

    public String getResponseType() {
        return responseType;
    }

    public String getClientId() {
        return clientId;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public String getResource() {
        return resource;
    }

    public String getState() {
        return state;
    }

    public String getScope() {
        return scope;
    }

    public String getAudience() {
        return audience;
    }

    public String getCodeChallenge() {
        return codeChallenge;
    }

    public String getCodeChallengeMethod() {
        return codeChallengeMethod;
    }
}
