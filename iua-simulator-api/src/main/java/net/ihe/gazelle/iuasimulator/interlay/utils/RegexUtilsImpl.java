package net.ihe.gazelle.iuasimulator.interlay.utils;


import net.ihe.gazelle.iuasimulator.application.utils.RegexUtils;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 13/04/2023
 */
public class RegexUtilsImpl implements RegexUtils {

    @Override
    public boolean isValidPersonIdFormat(String value) {
        String pattern = "^[-a-zA-Z0-9._~+\\/]{1,36}\\^{3}&\\;2.16.756.5.30.1.127.3.10.3\\&\\;ISO$";
        return value.matches(pattern);
    }

    @Override
    public boolean isValidPurposeOfUseFormat(String value) {
        return value.matches("^(urn:oid:2.16.756.5.30.1.127.3.10.5\\|((AUTO|NORM|EMER)$))$");
    }

    @Override
    public boolean isValidSubjectRoleFormat(String value) {
        return value.matches("^urn:oid:2.16.756.5.30.1.127.3.10.6\\|((PAT|HCP|REP|ASS)$)$");
    }

    @Override
    public boolean isValidGroupIdFormat(String value) {
        return value.matches("urn:oid:(\\d\\.){3}\\d$");
    }

    @Override
    public boolean isValidPrincipalIdFormat(String value) {
        return value.matches( "\\d{13}");
    }

    @Override
    public boolean isValidAccessTokenFormat(String value) {
        return value.equals("ihe-jwt") || value.equals("ihe-saml");
    }

    @Override
    public boolean isValidResponseTypeValue(String value) {
        return value.equals("code");
    }
    @Override
    public boolean isValidCodeChallengeMethodValue(String value) {
        return value.equals("S256");
    }

    @Override
    public boolean isValidCodeChallengeFormat(String value) {
        return value.matches(".{43}");
    }

    @Override
    public boolean isValidURL(String url) {
        String pattern = "https?:\\/\\/(?:w{1,3}\\.)?[^\\s.]+(?:\\.[a-z]+)*+(?::\\d+)?.*";
        return url.matches(pattern);
    }
}