package net.ihe.gazelle.iuasimulator.application.mapper;

import java.util.Map;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 25/04/2023
 */
public interface IuaScopeMapper<T> {
    T getScopeFromQueryParam(String scope);
    Map<String, String> getScopeMap(T param);
}
