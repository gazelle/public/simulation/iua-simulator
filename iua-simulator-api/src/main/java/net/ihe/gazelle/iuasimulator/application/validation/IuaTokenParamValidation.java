package net.ihe.gazelle.iuasimulator.application.validation;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 17/04/2023
 */
public interface IuaTokenParamValidation {
    String getGrantTypeRequired();

    String getCodeRequired();

    String getCodeVerifierRequired();

    String getRedirectUriRequired();

    String getUrlFormat();

    String getRedirectUriValue();
}
