package net.ihe.gazelle.iuasimulator.domain.validation;

import net.ihe.gazelle.iuasimulator.application.validation.IuaTokenParamValidation;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 17/04/2023
 */
public abstract class IuaTokenParamValidationImpl implements IuaTokenParamValidation {

    private String grantTypeRequired;
    private String codeRequired;
    private String codeVerifierRequired;
    private String redirectUriRequired;
    private String urlFormat;
    private String redirectUriValue;

    @Override
    public String getGrantTypeRequired() {
        return grantTypeRequired;
    }
    @Override
    public String getCodeRequired() {
        return codeRequired;
    }
    @Override
    public String getCodeVerifierRequired() {
        return codeVerifierRequired;
    }
    @Override
    public String getRedirectUriRequired() {
        return redirectUriRequired;
    }
    @Override
    public String getUrlFormat() {
        return urlFormat;
    }
    @Override
    public String getRedirectUriValue() {
        return redirectUriValue;
    }

}
