# CH IUA Authorization Server Simulator

___

The goal of this document is to help deploy a simulator for the IUA (Internet User Authorization) Profile concording to
both the IHE and CH specification, and give an overlook of what such a tool should be able to do.

# Table of Contents

1. [Installation process](#installation-process)
2. [Specifications](#specifications)
   2.1 [ITI-71](#iti-71)
   2.2 [ITI-72](#iti-72)
   2.3 [ITI-102](#iti-102)
   2.4 [ITI-103](#iti-103)

___

# Installation process <a name="installation-process"></a>

#### Prérequisites :

Either :

- Java 17
- Wildfly server 27
- A Keycloak server open at port 8090

or

- Docker

## Docker Installation

In this project you'll find a docker compose file containing everything you need.
You just need to execute this file to have your environment setup

```cmd
docker-compose command line to write
```

- A Keycloak server deployed at address localhost:8080
- A wildfly running with the authorization server available at address localhost:8080

All other endpoints needed to understand the workflow of the simulator will be explained later in this document.

## Configuration

### Environment variables
| Name                                            | Description                                           | Example                                                |
|-------------------------------------------------|-------------------------------------------------------|--------------------------------------------------------|
| REALMS_CONF_TEST_IUA_JWT_ENDPOINT               | Keycloak endpoint for test realm access               | http://localhost:8090/iua-sso/realms/test-iua              |
| REALMS_CONF_TEST_IUA_CLIENT_SECRET              | Client secret for test realm                          | OXoqU7IfigrmnFTAq78MzhupGI0m14W1                       |
| REALMS_CONF_TEST_IUA_CLIENT_ID                  | Client id for integrate scopes                        | 489070b4-0f6b-4859-b2e9-16da2194e180                   |
| REALMS_CONF_TEST_IUA_CLIENT_NAME                | Client name for test realm (client_id in keycloak ui) | test-iua-client                                        |
| REALMS_CONF_TEST_IUA_CLIENT_HUMAN_NAME          | Human readable name                                   | TEST IUA CLIENT JWT                                    |
| REALMS_CONF_TEST_IUA_SERVER_CALLBACK            | Valid redirect URIs for test client                   | http://localhost:8080/iua-simulator/rest/test/callback |
| REALMS_CONF_TEST_IUA_PRIVILEGED_USER_USERNAME   | Name for privileged user in test realm                | ihe_privileged_user                                    |
| REALMS_CONF_TEST_IUA_PRIVILEGED_USER_CREDENTIAL | Password for privileged user in test realm            | ihe_privileged_password                                |
| REALMS_CONF_TEST_IUA_USER_USERNAME              | Password for user in test realm                       | pvm                                                    |
| REALMS_CONF_TEST_IUA_USER_CREDENTIAL            | Password for user in test realm                       | password                                               |
|                                                 |                                                       |                                                        |
| REALMS_CONF_IHE_IUA_JWT_ADMIN_ENDPOINT          | Keycloak endpoint for admin access                    | http://localhost:8090/iua-sso/admin/realms/ihe-iua         |
| REALMS_CONF_IHE_IUA_JWT_ENDPOINT                | Keycloak endpoint for ihe realm access                | http://localhost:8090 /iua-sso/realms/ihe-iua              |
| REALMS_CONF_IHE_IUA_JWT_SECRET                  | Client secret for ihe realm                           | 33BWXUt48RFFpL2yQ3ZRs4YmpQyQ1WBY                       |
| REALMS_CONF_IHE_IUA_CLIENT_ID                   | Client ID for ihe realm                               | 2e3049ce-3aee-40a1-9636-44b28f32d930                   |
| REALMS_CONF_IHE_IUA_CLIENT_NAME                 | Client name for ihe realm                             | iua-ihe-client                                         |
| REALMS_CONF_IHE_IUA_CLIENT_HUMAN_NAME           | Human readable name                                   | IHE IUA CLIENT JWT                                     |
| REALMS_CONF_IHE_IUA_SERVER_CALLBACK             | Valid redirect URIs for ihe client                    | http://localhost:8080/iua-simulator/rest/ihe/callback  |
| REALMS_CONF_IHE_IUA_PRIVILEGED_USER_USERNAME    | Name for privileged user in test realm                | ihe_privileged_user                                    |
| REALMS_CONF_IHE_IUA_PRIVILEGED_USER_CREDENTIAL  | Password for privileged user in test realm            | ihe_privileged_password                                |
|                                                 |                                                       |                                                        |
| REALMS_CONF_SPE_IUA_JWT_ADMIN_ENDPOINT          | Keycloak endpoint admin for spe realm access          | http://localhost:8090/iua-sso/admin/realms/spe-iua         |
| REALMS_CONF_SPE_IUA_JWT_ENDPOINT                | Keycloak endpoint for test realm access               | http://localhost:8090/iua-sso/realms/spe-iua               |
| REALMS_CONF_SPE_IUA_JWT_SECRET                  | Client secret for spe realm                           | vMLfCAjAa9OiuQoK21EUN66R8fbx2fxa                       |
| REALMS_CONF_SPE_IUA_CLIENT_ID                   | Client ID for spe realm                               | 3d59b3b0-1ede-4eda-8f3a-a2fd22520a9b                   |
| REALMS_CONF_SPE_IUA_CLIENT_NAME                 | Client name for spe realm                             | spe-iua-client                                         |
| REALMS_CONF_SPE_IUA_CLIENT_HUMAN_NAME           | Human readable name                                   | CH IUA CLIENT JWT                                      |
| REALMS_CONF_SPA_IUA_SERVER_CALLBACK             | Valid redirect URIs for spe client                    | http://localhost:8080/iua-simulator/rest/spe/callback  |
| REALMS_CONF_SPE_IUA_LDAP_CONNECTION_URL         | Url for Ldap connection                               | ldap://host.ihe-europe.net:61560                       |
| REALMS_CONF_SPE_IUA_LDAP_BIND_CREDENTIAL        | Credential for Ldap connection                        | secret                                                 |
| REALMS_CONF_SPE_IUA_PRIVILEGED_USER_USERNAME    | privileged user name                                  | privileged_user                                        |
| REALMS_CONF_SPE_IUA_PRIVILEGED_USER_CREDENTIAL  | privileged user password                              | privileged_password                                    |      

## Applications deployment (for dev)

### Deploy with Maven

The first possibility is to deploy GUM with maven-docker-plugin. This process is useful for integrations tests.

```bash
mvn -f iua-simulator-service/pom.xml docker:start
```

The list of applications that will be deployed :

- gazelle-database (https://gitlab.inria.fr/gazelle/private/industrialization/docker/gazelle-database)
-
gazelle-keycloak (https://gitlab.inria.fr/gazelle/private/industrialization/docker/gazelle-keycloak/-/tree/keycloak_v_20)
- iua-simulator

#### Check the logs

```bash
docker logs -f iua-simulator
```

Tips : you can add the flag __-Ddocker.follow__ to your command to automatically follow the logs.

### Stop the services

```bash
mvn -f iua-simulator-service/pom.xml docker:stop
```

## Java Installation

Place yourself in the root directory of this project, then on a terminal, run the command
```cmd
mvn clean install -DskipTests
```

An artifact will be created in the iua-simulator-service directory called iua-simulator-service.war

Copy this artifact in your wildfly standalone/deployments/ folder and start your server.

You'll then be able to access your simulator through the address setup in your wildfly configuration file.
___

# Specifications

As explained in the specification that can be found on [profiles.ihe.net](#https://profiles.ihe.net/ITI/IUA/index.html),
IUA is a way to add Authorization to profiles and transaction through the use of an Authorization Server.
Authorization Clients are used to interact with the Authorization Server to retrieve access token. Tokens then used in
HTTP Rest transactions to access resources deployed on Servers.
You'll find here a list of all transactions available through this simulator.
___

## ITI-71 <a name="iti-71"></a>

The ITI-71 transaction is sent by the Authorization Client to the Authorization Server to retrieve an access Token to be
used within requests to the Resource server.

Two ways can be used to resolve this transaction, but the one used by this simulator is the Authorization Code grant
type.

![](./resources/iti-71.png)

The first Http Request is a GET containing the following parameters :

| parameter            | required/optional | description                                                                                                                                                                                                                |
|----------------------|-------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| response_type        | required          | The response format of the authorization method. Here must be code                                                                                                                                                         |
| client_id            | required          | The ID, the client is registered at the IUA Authorization Server.                                                                                                                                                          |
| redirect_uri         | required          | Used as callback URL, the IUA Authorization Server will send the authorization code to. The URL SHALL match one of the client’s pre-registered redirect URIs.                                                              |
| state                | required          | An unguessable value used by the client to track the state between the authorization request and the callback.                                                                                                             |
| scope                | required          | Attributes the app claims                                                                                                                                                                                                  |
| aud                  | required          | The audience URL the token will be used for.                                                                                                                                                                               |
| launch               | optional          | If present, the launch parameter indicates that the app (or the view) was launched from an EHR or mHealth App context which has already been authorized to access the Swiss EPR (e.g. SMART on FHIR based primary system). |
| code_challenge       | required          | Transformed version of code_verifier with code_challenge_method                                                                                                                                                            |
| code_chalenge_method | required          | SHALL be “S256”.                                                                                                                                                                                                           |

This request must be sent to the /authorize endpoint of the authorization server.
This means the request should look like something like this :

``` http request
HTTP GET {$simulator.endpoint}/iua-simulator/rest/ch/authorize?
   response_type=code
   &client_id=ch-iua-client
   &state=helloFromChIua
   &resource=ihe.pixm.net
   &code_challenge=fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg
   &code_challenge_method=S256
   &redirect_uri={$simulator.endpoint}/iua-simulator/rest/ch/callback
   &scope=profile
   &aud=http://pixmResourceServerURL.ch
   &launch=azerty
   HTTP 1.1
```

Following this request, you will receive an Authorization Code in a JSON formatted response.

Using the Authorization client, you will then need to do a second Request:

| parameter     | required/optional                | description                                                                                             |
|---------------|----------------------------------|---------------------------------------------------------------------------------------------------------|
| client_id     | required in Authorization header | The ID the client is registered at the IUA Authorization Server.                                        |
| client_secret | required in Authorization header | The secret to access the client                                                                         |
| redirect_uri  | required                         | The URI to redirect the apps user agent to. Must be the same as in the first request                    |
| grant_type    | required                         | Value shall be “authorization_code”.                                                                    |
| code          | required                         | The authorization code we retrieved through the previous request                                        |
| code_verifier | required                         | The original code verifier string, the non-hashed version of the code_challenge from! the first request |

This request is a Post sent to the /token endpoint of the authorization server.
Those arguments must therefore be contained in a body as an *application/x-www-form-urlencoded* content-type, and
encoded in UTF-8
Finally both *client_id* and *client_secret* must be encapsulated in the base HTTP Authorization header.

This means the request should like something like this :

``` http request
HTTP POST {$simulator.endpoint}/iua-simulator/rest/ch/token HTTP 1.1
Authorization: Basic vsdvZRTYHQS51SgvtsqeGFs62
Content-Type: application/x-www-form-urlencoded

grant_type=authorization_code
&code=13fER1Gqe2FshWDRwg
&redirect_uri={$simulator.endpoint}/iua-simulator/rest/ch/callback
&code_verifier=-5Q635ku8ER2pfGeuDagpsk40
```

As a response to this, you should receive a JSON containing the access_token. You can use resources such
as [jwt.io](#https://jwt.io)
to verify the content of your access_token.

This access_token can now be used by the user to access resource servers or for other transactions.
___

## ITI-72 <a name="iti-72"></a>

Coming soon

___

## ITI-102 <a name="iti-102"></a>

Not in scope

___

## ITI-103 <a name="iti-103"></a>

The ITI-103 request allows for interaction between the authorization server and resources server.

The content returned by this page is the following :

| Name   | Required/Optional ? | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
|--------|----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| issuer | required | The Authorization Server's issuer identifier, which is a URL that uses the "https" scheme and has no query or fragment components. The URL shall use the "https" scheme. The URL shall contain the domain name of the URL at which the metadata document can be retrieved. The URL shall contain any path elements referring to a tenant. Other path elements, such as those referring to the document location (e.g., ".well-known" or "openid-configuration") should be omitted, e.g., a valid issuer associated with a metadata document retrievable at https://example.com/.well-known/oauth-authorization-server/tenant1 would be https://example.com/tenant1. |
|authorization_endpoint | required | Authorization Server's Authorization endpoint as used for the "authorization code" flow.|
|token_endpoint | required | Authorization Server's Authorization token endpoint location.|
|jwks_uri | conditional | URL of the Authorization Server's JWK Set [RFC7517, Section 5] document. An Authorization Server that supports the JWT Token or SAML Token Options shall provide this claim to communicate the public keys that can be used to verify JWT token or SAML token signatures.|
|scopes_supported | optional | The list of scopes supported by the Authorization Server. Note this transaction does not define the scopes. Authorization Servers may opt to publish a subset of the scopes usable.|
|response_types_supported | required | As the implicit grant flow is not supported in OAuth2.1, the response types should not include the value "token". Authorization Servers shall include the response type "code". Authorization Servers supporting [OpenId Connect] or other standards may include other token types, such as "id_token".|
|grant_types_supported | required | A JSON array listing the type of grants supported toward the token endpoint. The list shall include the values "client_credentials", and "authorization_code". Authorization Servers supporting refresh tokens shall include "refresh-token". Authorization Servers supporting the JWT grant shall include "urn:ietf:params:oauth:grant-type:jwt-bearer".Authorization Servers supporting the SAML grant shall include "urn:ietf:params:oauth:grant-type:saml2-bearer". Authorization Servers supporting other types of grants should provide additional values to this list representing those grant types.|
|token_endpoint_auth_methods_supported | optional | JSON array containing a list of client authentication methods supported by this token endpoint. When provided, this list shall include "client_secret_basic".|
|introspection_endpoint | conditional |URL of the Authorization Server's OAuth 2.0 introspection endpoint. This claim must be provided by Authorization Servers supporting the Token Introspection Option.|
|introspection_endpoint_auth_methods_supported | conditional | JSON array containing a list of client authentication methods supported by the introspection endpoint. This claim must be provided by Authorization Servers supporting the Token Introspection Option. As the introspect option mandates support for Resource Server bearer access tokens, this claim must include the "Bearer" value. Other authentication methods (e.g., "client_secret_basic") are allowed when supported by the Authorization Server.|
|access_token_format | optional | JSON string defining the format of the access token as provided by the Authorization Server. This allows Resource Servers to learn about methods of verification. Authorization Servers supporting the JWT Token Option shall set this claim value to "ihe-jwt". Authorization Servers supporting the SAML Token Option shall set this claim value to "ihe-saml". Authorization Servers providing tokens that are non-parsable shall set this value to "opaque". Authorization Servers supporting access tokens in other structured formats may use alternative format identifiers.|


``` http request
HTTP GET {$simulator.endpoint}/iua-simulator/rest/ch/.well-known/smart-configuration HTTP 1.1

```