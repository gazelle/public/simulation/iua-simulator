package net.ihe.gazelle.iuasimulator.client.it;

import jakarta.ws.rs.core.Response;
import net.ihe.gazelle.iuasimulator.client.interlay.keycloak.KeycloakClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**redirectUri
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 03/02/2023
 */
class KeycloakClientIT {
    private String endPoint;
    private String clientId;
    private String redirectUri;
    private KeycloakClient keycloakClient;

    // TODO: Check all Keycloak IT !!!

    @BeforeEach
    void init() {
        endPoint = "http://localhost:8090/iua-sso/realms/ihe-iua";
        clientId = "ihe-iua-client";
        redirectUri = "http://localhost:8080/iua-simulator/rest/ihe/callback";
        keycloakClient = new KeycloakClient(endPoint);
    }

    @Test
    void testAuthCode() {
        Response response = keycloakClient.requestAuthCodeEndpoint(
                "code",
                clientId,
                "helloFromIheIua",
                "",
                "fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg",
                "S256",
                redirectUri,
                "profile");
        Assertions.assertEquals(200, response.getStatus());
    }

    @Test
    void requestServerMetadataTest() {
        Response response = keycloakClient.requestServerMetadata();
        Assertions.assertEquals(200, response.getStatus());
    }
}