package net.ihe.gazelle.iuasimulator.client.application;

import jakarta.ws.rs.core.Response;

public interface AuthenticationClient {

    Response requestSamlToken(String clientId, String clientSecret);

    Response requestTokenEndpoint(String header, String clientId, String clientSecret, String username, String password, String clientSessionState);

    Response requestAuthCodeEndpoint(
            String responseType,
            String clientId,
            String state,
            String resource,
            String codeChallenge,
            String codeChallengeMethod,
            String redirectUri,
            String scope);


    Response requestServerMetadata();

    Response exchangeAuthorizationCode(String code, String clientSecret, String redirectUri, String clientId, String codeVerifier);

}
