package net.ihe.gazelle.iuasimulator.client.exceptions;

public class SamlGenerationException extends Exception {

    public SamlGenerationException(String message){
        super(message);
    }
    public SamlGenerationException(String message, Throwable cause) {
        super(message, cause);
    }

}
