package net.ihe.gazelle.iuasimulator.client.interlay.service;

import net.ihe.gazelle.iuasimulator.client.application.SamlService;
import net.ihe.gazelle.iuasimulator.client.exceptions.SamlGenerationException;
import net.ihe.gazelle.iuasimulator.xua.generator.model.AssertionAttributes;
import net.ihe.gazelle.iuasimulator.xua.generator.model.KeystoreParams;
import net.ihe.gazelle.iuasimulator.xua.generator.utils.AssertionUtils;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 28/04/2023
 */
public class SamlServiceImpl implements SamlService {
    private static final String DEFAULT_KEYSTORE_PATH = "/opt/gazelle/cert/iua.jks";
    private static final String DEFAULT_KEYSTORE_PASSWORD = "password";
    private static final String DEFAULT_KEY_ALIAS = System.getenv("REALMS_CONF_SPE_IUA_CLIENT_NAME");
    private static final String DEFAULT_KEY_PASSWORD = "password";
    private static final String DEFAULT_TRUSTSTORE_PATH = DEFAULT_KEYSTORE_PATH;
    private static final String DEFAULT_TRUSTSTORE_PASSWORD = DEFAULT_KEYSTORE_PASSWORD;

    @Override
    public String generateSAML(String group, String groupId, String launch, String principal, String personId, String principalId, String purposeOfUse, String subjectRole, String eprSpid) throws Exception {
        KeystoreParams keystoreParams = new KeystoreParams(DEFAULT_KEYSTORE_PATH, DEFAULT_KEYSTORE_PASSWORD, DEFAULT_TRUSTSTORE_PATH, DEFAULT_TRUSTSTORE_PASSWORD, DEFAULT_KEY_ALIAS, DEFAULT_KEY_PASSWORD);
        String purposeOfUseCode = (null != purposeOfUse) ? purposeOfUse.substring(purposeOfUse.length()-4, purposeOfUse.length()) : "";
        String subjectRoleCode = (null != subjectRole) ? subjectRole.substring(subjectRole.length()-3, subjectRole.length()) : "";
        AssertionAttributes assertionAttributes = new AssertionAttributes(eprSpid, personId, purposeOfUseCode, personId, groupId, group, subjectRoleCode , "", "", "", groupId, principalId, principal);
        try {
            String assertion = AssertionUtils.getStringAssertionWithPath(assertionAttributes, keystoreParams);
            if (assertion != null) {
                return assertion;
            } else {
                throw new SamlGenerationException("Failed to create Saml Assertion, the returned result is empty.");
            }
        } catch (Exception e) {
            throw new SamlGenerationException("Failed to create Saml Assertion, an error occured during creation.",e);
        }
    }
}

