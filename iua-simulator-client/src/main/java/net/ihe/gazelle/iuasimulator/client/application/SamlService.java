package net.ihe.gazelle.iuasimulator.client.application;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 28/04/2023
 */
public interface SamlService {
    String generateSAML(String b, String c, String d, String e, String f, String g, String h, String i, String eprSpid) throws Exception;
}
