package net.ihe.gazelle.iuasimulator.client.interlay.keycloak;

import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import net.ihe.gazelle.iuasimulator.client.application.AuthenticationClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

public class KeycloakClient implements AuthenticationClient {

    private static final String CLIENT_SECRET = "client_secret";
    private static final String CODE_GRANT_ENDPOINT = "/protocol/openid-connect/auth";
    private static final String TOKEN_ENDPOINT = "/protocol/openid-connect/token";
    private static final String CERTS_ENDPOINT = "/protocol/openid-connect/certs";
    private static final String METADATA_ENDPOINT = "/.well-known/openid-configuration";
    private static final String CLIENTS = "/clients";
    private static final String GRANT_TYPE = "grant_type";
    private static final String REDIRECT_URI = "redirect_uri";
    private static final String AUTHORIZATION = "Authorization";
    private static final String SCOPE = "scope";
    private static final String CLIENT_ID = "client_id";
    public static final String BEARER = "Bearer ";
    private final ResteasyClient keycloakApiClient = (ResteasyClient) ClientBuilder.newBuilder().build();
    private final String endpoint;

    public KeycloakClient(String endpoint){
        this.endpoint = endpoint;
    }

    @Override
    public Response requestSamlToken(String clientId, String clientSecret) {
        ResteasyWebTarget target = keycloakApiClient.target(endpoint + TOKEN_ENDPOINT)
                .queryParam(CLIENT_ID, clientId)
                .queryParam(CLIENT_SECRET, clientSecret);
        return target.request().get();
    }

    @Override
    public Response requestTokenEndpoint(String header, String redirectUri, String grantType, String codeVerifier, String code, String clientSessionState) {
        ResteasyWebTarget target = keycloakApiClient.target(endpoint + TOKEN_ENDPOINT);
        Form form = new Form();
        form.param(GRANT_TYPE, grantType);
        form.param(REDIRECT_URI, redirectUri);
        form.param("code_verifier", codeVerifier);
        form.param("code", code);
        form.param("client_session_state", clientSessionState);
        Entity<Form> entity = Entity.form(form);
        return target.request(MediaType.APPLICATION_FORM_URLENCODED).header(AUTHORIZATION, header).post(entity);
    }

    public Response requestAuthCodeEndpoint(String responseType,
                                            String clientId,
                                            String redirectUri,
                                            String state,
                                            String scope,
                                            String audience,
                                            String launch,
                                            String codeChallenge,
                                            String codeChallengeMethod)
    {
        ResteasyWebTarget target = keycloakApiClient.target(endpoint + CODE_GRANT_ENDPOINT)
                .queryParam("code_challenge", codeChallenge)
                .queryParam("code_challenge_method", codeChallengeMethod)
                .queryParam(REDIRECT_URI, redirectUri)
                .queryParam(SCOPE, scope)
                .queryParam("state", state)
                .queryParam("response_type", responseType)
                .queryParam("audience", audience)
                .queryParam("launch", launch)
                .queryParam(CLIENT_ID, clientId);
        return target.request().get();
    }

    @Override
    public Response requestAuthCodeEndpoint(String responseType,
                                            String clientId,
                                            String state,
                                            String resource,
                                            String codeChallenge,
                                            String codeChallengeMethod,
                                            String redirectUri,
                                            String scope
    ) {
        ResteasyWebTarget target = keycloakApiClient.target(endpoint + CODE_GRANT_ENDPOINT)
                .queryParam("code_challenge", codeChallenge)
                .queryParam("code_challenge_method", codeChallengeMethod)
                .queryParam(REDIRECT_URI, redirectUri)
                .queryParam(SCOPE, scope)
                .queryParam("state", state)
                .queryParam("response_type", responseType)
                .queryParam(CLIENT_ID, clientId);
        return target.request().get();
    }

    @Override
    public Response exchangeAuthorizationCode(String code, String clientSecret, String redirectUri, String clientId, String codeVerifier) {
        ResteasyWebTarget target = keycloakApiClient.target(endpoint + TOKEN_ENDPOINT);
        Form form = new Form();
        form.param(CLIENT_SECRET, clientSecret);
        form.param(REDIRECT_URI, redirectUri);
        form.param(CLIENT_ID, clientId);
        form.param(GRANT_TYPE, "authorization_code");
        form.param("code_verifier", codeVerifier);
        form.param("code", code);
        Entity<Form> entity = Entity.form(form);
        return target.request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);
    }

    @Override
    public Response requestServerMetadata() {
        ResteasyWebTarget target = keycloakApiClient.target(endpoint + METADATA_ENDPOINT);
        return target.request().get();
    }

    public Response requestServerCerts() {
        ResteasyWebTarget target = keycloakApiClient.target(endpoint + CERTS_ENDPOINT);
        return target.request().get();
    }

    public Response getClient(){
        ResteasyWebTarget target = keycloakApiClient.target(endpoint + CLIENTS);
        return target.request(MediaType.APPLICATION_JSON_TYPE).get();
    }

    public Response logoutRequest(){
        ResteasyWebTarget target = keycloakApiClient.target(endpoint + "/protocol/openid-connect/logout");
        Form form = new Form();
        form.param("client_session_state", "helloFromChIua");
        Entity<Form> entity = Entity.form(form);
        return target.request(MediaType.APPLICATION_FORM_URLENCODED).post(entity);
    }
}