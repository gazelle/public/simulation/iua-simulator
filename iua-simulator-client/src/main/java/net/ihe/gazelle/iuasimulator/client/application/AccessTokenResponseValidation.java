package net.ihe.gazelle.iuasimulator.client.application;

import jakarta.ws.rs.core.Response;

import java.io.IOException;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 28/04/2023
 */
public interface AccessTokenResponseValidation {
    String getScopeFromResponse(Response response) throws IOException;

    Response formatSamlResponse(Response response, String assertion) throws IOException;

    String getUserSpidFromResponse(Response response) throws IOException;

    String getUserSubFromResponse(Response response) throws IOException;
    String getUserSubFromToken(String token) throws IOException;
}
