package net.ihe.gazelle.iuasimulator.client.interlay.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import jakarta.ws.rs.core.Response;
import net.ihe.gazelle.iuasimulator.client.application.AccessTokenResponseValidation;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 27/04/2023
 */
public class AccessTokenResponseValidationImpl implements AccessTokenResponseValidation {
    private static final String ACCESS_TOKEN = "access_token";
    private final ObjectMapper mapper = new ObjectMapper();


    /**
     * Return access_token's payload part.
     * @param response response
     * @return payload decoded
     * @throws IOException IOException
     */
    @Override
    public String getScopeFromResponse(Response response) throws IOException {
        String token = response.readEntity(String.class);
        JsonNode tokenNode = mapper.readTree(token);
        String payload = tokenNode.get("scope").asText();
        tokenNode.path("scope");
        return payload;
    }

    @Override
    public Response formatSamlResponse(Response response, String assertion) throws IOException {
        String token = response.readEntity(String.class);
        ObjectNode tokenNode = (ObjectNode) mapper.readTree(token);
        tokenNode.remove(ACCESS_TOKEN);
        tokenNode.put(ACCESS_TOKEN, Base64.getEncoder().encodeToString(assertion.getBytes(StandardCharsets.UTF_8)));
        return Response.accepted().entity(tokenNode).header("Access-Control-Allow-Origin", "*").build();
    }

    @Override
    public String getUserSpidFromResponse(Response response) throws IOException {
        String token = response.readEntity(String.class);
        JsonNode tokenNode = mapper.readTree(token);
        String[] payload = tokenNode.get(ACCESS_TOKEN).asText().split("\\.");
        String decodedPayload = new String(Base64.getDecoder().decode(payload[1]));
        JsonNode payloadNode = mapper.readTree(decodedPayload);
        JsonNode userId = payloadNode.get("extensions").get("ch_epr").get("user_id");
        return userId.asText();
    }

    @Override
    public String getUserSubFromResponse(Response response) throws IOException {
        String token = response.readEntity(String.class);
        JsonNode tokenNode = mapper.readTree(token);
        String[] payload = tokenNode.get(ACCESS_TOKEN).asText().split("\\.");
        return getUserSubFromToken(payload[1]);
    }

    @Override
    public String getUserSubFromToken(String token) throws IOException {
        String decodedPayload = new String(Base64.getDecoder().decode(token));
        JsonNode payloadNode = mapper.readTree(decodedPayload);
        return payloadNode.get("sub").asText();
    }

}
