package net.ihe.gazelle.iuasimulator.xua.generator.keystoreutils;


import net.ihe.gazelle.iuasimulator.xua.generator.model.KeystoreParams;
import net.ihe.gazelle.iuasimulator.xua.generator.utils.SMgrException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

/**
 * @author jerouris
 */
public final class DefaultKeyStoreManager implements KeyStoreManager {
    public static final String KEY_WITH_ALIAS = "Key with alias:";

    private String keystoreLocation;
    private String truststoreLocation;
    private String keystorePassword;
    private String truststorePassword;
    private String privatekeyAlias;
    private String privatekeyPassword;
    private KeyStore keyStore;
    private KeyStore trustStore;

    public DefaultKeyStoreManager(KeystoreParams params) {
        this.keystoreLocation = params.getKeystorePath();
        this.truststoreLocation = params.getTruststorePath();
        this.keystorePassword = params.getKeystorePassword();
        this.truststorePassword = params.getPrivateKeyPassword();
        this.privatekeyAlias = params.getKeyAlias();
        this.privatekeyPassword = params.getPrivateKeyPassword();
    }

    public void generateStores() throws Exception {
        this.keyStore = getKeyStore();
        this.trustStore = getTrustStore();
    }


    public KeyPair getPrivateKey(String alias, char[] password) throws SMgrException {

        try {

            // Get private key
            Key key = keyStore.getKey(alias, password);
            if (key instanceof PrivateKey privateKey) {
                // Get certificate of public key
                Certificate cert = keyStore.getCertificate(alias);

                // Get public key
                PublicKey publicKey = cert.getPublicKey();

                // Return a key pair
                return new KeyPair(publicKey, privateKey);
            }
        } catch (UnrecoverableKeyException e) {
            throw new SMgrException(KEY_WITH_ALIAS + alias + " is unrecoverable", e);
        } catch (NoSuchAlgorithmException e) {
            throw new SMgrException(KEY_WITH_ALIAS + alias + " uses an incompatible algorithm", e);
        } catch (KeyStoreException e) {
            throw new SMgrException(KEY_WITH_ALIAS + alias + " not found", e);
        }
        return null;
    }

    public KeyStore getKeyStore() throws Exception {
        keyStore = buildKeyStore(keystoreLocation, keystorePassword);
        return keyStore;
    }

    public Certificate getCertificate(String alias) throws SMgrException {
        try {
            return keyStore.getCertificate(alias);
        } catch (KeyStoreException ex) {
            throw new SMgrException("Certificate with alias: " + alias + " not found in keystore", ex);
        }
    }

    public KeyStore getTrustStore() throws Exception {
        trustStore = buildKeyStore(truststoreLocation, truststorePassword);
        return trustStore;
    }

    public KeyPair getDefaultPrivateKey() throws SMgrException {
        return getPrivateKey(privatekeyAlias, privatekeyPassword.toCharArray());
    }

    public Certificate getDefaultCertificate() throws SMgrException {
        return getCertificate(privatekeyAlias);
    }

    private KeyStore buildKeyStore(String location, String password) throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException {
        try (InputStream keystoreStream = new FileInputStream(location)){
            KeyStore store = KeyStore.getInstance(KeyStore.getDefaultType());
            store.load(keystoreStream, password.toCharArray());
            return store;
        }
    }
}