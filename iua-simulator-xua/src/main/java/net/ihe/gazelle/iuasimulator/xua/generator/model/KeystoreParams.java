package net.ihe.gazelle.iuasimulator.xua.generator.model;

/**
 * <p>KeystoreParams class.</p>
 *
 * @author aberge
 * @version 1.0: 26/09/17
 */

public class KeystoreParams {

    private String keystorePath;
    private String privateKeyPassword;
    private String keyAlias;
    private String truststorePath;
    private String truststorePassword;
    private String keystorePassword;

    public KeystoreParams(String inKeystorePath, String inKeystorePassword, String inTruststorePath, String inTruststorePassword, String inKeyAlias,
                          String inPrivateKeyPassword) {
        this.keyAlias = inKeyAlias;
        this.keystorePath = inKeystorePath;
        this.privateKeyPassword = inPrivateKeyPassword;
        this.truststorePath = inTruststorePath;
        this.truststorePassword = inTruststorePassword;
        this.keystorePassword = inKeystorePassword;
    }

    public String getKeystorePath() {
        return keystorePath;
    }

    public void setKeystorePath(String keystorePath) {
        this.keystorePath = keystorePath;
    }

    public String getPrivateKeyPassword() {
        return privateKeyPassword;
    }

    public void setPrivateKeyPassword(String privateKeyPassword) {
        this.privateKeyPassword = privateKeyPassword;
    }

    public String getKeyAlias() {
        return keyAlias;
    }

    public void setKeyAlias(String keyAlias) {
        this.keyAlias = keyAlias;
    }

    public String getTruststorePath() {
        return truststorePath;
    }

    public void setTruststorePath(String truststorePath) {
        this.truststorePath = truststorePath;
    }

    public String getTruststorePassword() {
        return truststorePassword;
    }

    public void setTruststorePassword(String truststorePassword) {
        this.truststorePassword = truststorePassword;
    }

    public String getKeystorePassword() {
        return keystorePassword;
    }

    public void setKeystorePassword(String keystorePassword) {
        this.keystorePassword = keystorePassword;
    }
}
