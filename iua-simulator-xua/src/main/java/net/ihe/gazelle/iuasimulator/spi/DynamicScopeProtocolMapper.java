package net.ihe.gazelle.iuasimulator.spi;

import java.util.ArrayList;
import java.util.List;
import org.keycloak.models.ClientSessionContext;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.ProtocolMapperModel;
import org.keycloak.models.UserSessionModel;
import org.keycloak.protocol.oidc.mappers.AbstractOIDCProtocolMapper;
import org.keycloak.protocol.oidc.mappers.OIDCAccessTokenMapper;
import org.keycloak.protocol.oidc.mappers.OIDCAttributeMapperHelper;
import org.keycloak.protocol.oidc.mappers.OIDCIDTokenMapper;
import org.keycloak.protocol.oidc.mappers.UserInfoTokenMapper;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.representations.IDToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DynamicScopeProtocolMapper extends AbstractOIDCProtocolMapper implements OIDCAccessTokenMapper, OIDCIDTokenMapper, UserInfoTokenMapper {
   private static final Logger LOGGER = LoggerFactory.getLogger(DynamicScopeProtocolMapper.class);
   public static final String PROVIDER_ID = "dynamic-mapper";
   private static final List<ProviderConfigProperty> configProperties = new ArrayList();

   public String getDisplayCategory() {
      return "Token Mapper";
   }

   public String getDisplayType() {
      return "CH:IUA Token scope";
   }

   public String getHelpText() {
      return "Adds a CH:IUA scope to the claim";
   }

   public List<ProviderConfigProperty> getConfigProperties() {
      return configProperties;
   }

   public String getId() {
      return "dynamic-mapper";
   }

   protected void setClaim(IDToken token, ProtocolMapperModel mappingModel, UserSessionModel userSession, KeycloakSession keycloakSession, ClientSessionContext clientSessionCtx) {
      String fullScope = clientSessionCtx.getScopeString();
      LOGGER.warn("FULL SCOPE = " + clientSessionCtx.getScopeString());
      String protocolMapperInstanceName = mappingModel.getName();
      String claimValue = this.manageScope(fullScope, protocolMapperInstanceName);
      OIDCAttributeMapperHelper.mapClaim(token, mappingModel, claimValue);
   }

   private String manageScope(String fullScope, String protocolMapperInstanceName) {
      byte var4 = -1;
      switch(protocolMapperInstanceName.hashCode()) {
      case -2045540157:
         if (protocolMapperInstanceName.equals("subject_role_code")) {
            var4 = 2;
         }
         break;
      case -1812041682:
         if (protocolMapperInstanceName.equals("principal")) {
            var4 = 7;
         }
         break;
      case -1493540500:
         if (protocolMapperInstanceName.equals("purpose_of_use_code")) {
            var4 = 0;
         }
         break;
      case -71727896:
         if (protocolMapperInstanceName.equals("purpose_of_use_identifier")) {
            var4 = 1;
         }
         break;
      case 98629247:
         if (protocolMapperInstanceName.equals("group")) {
            var4 = 5;
         }
         break;
      case 506361563:
         if (protocolMapperInstanceName.equals("group_id")) {
            var4 = 6;
         }
         break;
      case 853187141:
         if (protocolMapperInstanceName.equals("person_id")) {
            var4 = 4;
         }
         break;
      case 910289612:
         if (protocolMapperInstanceName.equals("principal_id")) {
            var4 = 8;
         }
         break;
      case 1798696837:
         if (protocolMapperInstanceName.equals("subject_role_system")) {
            var4 = 3;
         }
      }

      switch(var4) {
      case 0:
         return retrieveScopeValueWithDelimiter(fullScope, "purpose_of_use=", 1);
         case 1:
            return retrieveScopeValueWithDelimiter(fullScope, "purpose_of_use=", 0);
         case 2:
            return retrieveScopeValueWithDelimiter(fullScope, "subject_role=", 1);
         case 3:
            return retrieveScopeValueWithDelimiter(fullScope, "subject_role=", 0);
         case 4:
            return retrieveScopeValue(fullScope, "person_id=");
         case 5:
            return retrieveScopeValue(fullScope, "group=");
         case 6:
            return retrieveScopeValue(fullScope, "group_id=");
         case 7:
            return retrieveScopeValue(fullScope, "principal=");
         case 8:
            return retrieveScopeValue(fullScope, "principal_id=");
         default:
         LOGGER.error("Custom scope protocol mapper is used for an unrecognised item");
         return "";
      }
   }

   private static String retrieveScopeValue(String fullScope, String str) {
      int personIndex = fullScope.indexOf(str);
      String personIdentifierSubScope = fullScope.substring(personIndex);
      String[] personIdentifierSplittedScopes = personIdentifierSubScope.split(" ");
      String[] personIdentifierScope = personIdentifierSplittedScopes[0].split(str);
      return personIdentifierScope[1];
   }

   private static String retrieveScopeValueWithDelimiter(String fullScope, String str, int x) {
      int codeIndex = fullScope.indexOf(str);
      String codeSubScope = fullScope.substring(codeIndex);
      String[] codeSplittedScopes = codeSubScope.split(" ");
      String[] codeScope = codeSplittedScopes[0].split(str);
      String[] codeScopeCode = codeScope[1].split("\\|");
      return codeScopeCode[x];
   }

   static {
      OIDCAttributeMapperHelper.addTokenClaimNameConfig(configProperties);
      OIDCAttributeMapperHelper.addIncludeInTokensConfig(configProperties, DynamicScopeProtocolMapper.class);
   }
}
