package net.ihe.gazelle.iuasimulator.xua.generator.model;

import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.impl.SubjectConfirmationDataImpl;
import org.opensaml.xml.XMLObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SubjectConfirmationDataWithName extends SubjectConfirmationDataImpl {

    private AttributeStatement attributeStatementName;

    public AttributeStatement getAttributeStatementName() {
        return attributeStatementName;
    }

    public void setAttributeStatementName(AttributeStatement attributeStatementName) {
        this.attributeStatementName = attributeStatementName;
    }

    public SubjectConfirmationDataWithName(String namespaceURI, String elementLocalName, String namespacePrefix) {
        super(namespaceURI, elementLocalName, namespacePrefix);
    }

    @Override
    /** {@inheritDoc} */
    public List<XMLObject> getOrderedChildren() {
        List<XMLObject> children = new ArrayList<XMLObject>(super.getOrderedChildren());
        children.add(this.getAttributeStatementName());
        return Collections.unmodifiableList(children);
    }

}
