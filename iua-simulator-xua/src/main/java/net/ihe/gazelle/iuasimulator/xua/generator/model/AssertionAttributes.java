package net.ihe.gazelle.iuasimulator.xua.generator.model;

/**
 * <p>AssertionAttributes class.</p>
 *
 * @author aberge
 * @version 1.0: 26/09/17
 */

public class AssertionAttributes {

   static final String RECIPIENT = "https://sp.community.ch/epd";

   private String accessingPersonId;
   private String accessingPersonName;
   private String purposeOfUse;
   private String accessingPersonOrganizationId;
   private String accessingPersonOrganizationName;
   private String accessingPersonRole;
   private String requestedResourceId;
   private String appliesTo;
   private String subjectConfirmationId;
   private String subjectConfirmationRole;
   private String subjectConfirmationName;
   private String homeCommunityId;

   private String principalId;
   private String principalName;

   public AssertionAttributes(String personId, String resourceId, String inPurposeOfUse, String personName, String organizationId, String
         organizationName, String personRole, String subjectConfirmationId, String subjectConfirmationRole, String subjectConfirmationName,
                              String homeCommunityId, String principalId, String principalName) {
      setAccessingPersonId(personId);
      setRequestedResourceId(resourceId);
      setPurposeOfUse(inPurposeOfUse);
      setAccessingPersonName(personName);
      setAccessingPersonOrganizationId(organizationId);
      setAccessingPersonOrganizationName(organizationName);
      setAccessingPersonRole(personRole);
      setAppliesTo(RECIPIENT);
      setSubjectConfirmationId(subjectConfirmationId);
      setSubjectConfirmationRole(subjectConfirmationRole);
      setSubjectConfirmationName(subjectConfirmationName);
      setHomeCommunityId(homeCommunityId);
      setPrincipalId(principalId);
      setPrincipalName(principalName);
   }

   public String getAccessingPersonId() {
      return accessingPersonId;
   }

   public void setAccessingPersonId(String accessingPersonId) {
      this.accessingPersonId = accessingPersonId;
   }

   public String getPurposeOfUse() {
      return purposeOfUse;
   }

   public void setPurposeOfUse(String purposeOfUse) {
      this.purposeOfUse = purposeOfUse;
   }

   public String getAccessingPersonOrganizationId() {
      return accessingPersonOrganizationId;
   }

   public void setAccessingPersonOrganizationId(String accessingPersonOrganizationId) {
      this.accessingPersonOrganizationId = accessingPersonOrganizationId;
   }

   public String getAccessingPersonOrganizationName() {
      return accessingPersonOrganizationName;
   }

   public void setAccessingPersonOrganizationName(String accessingPersonOrganizationName) {
      this.accessingPersonOrganizationName = accessingPersonOrganizationName;
   }

   public String getAccessingPersonRole() {
      return accessingPersonRole;
   }

   public void setAccessingPersonRole(String accessingPersonRole) {
      this.accessingPersonRole = accessingPersonRole;
   }

   public String getRequestedResourceId() {
      return requestedResourceId;
   }

   public void setRequestedResourceId(String requestedResourceId) {
      this.requestedResourceId = requestedResourceId;
   }

   public String getAccessingPersonName() {
      return accessingPersonName;
   }

   public void setAccessingPersonName(String accessingPersonName) {
      this.accessingPersonName = accessingPersonName;
   }

   public String getAppliesTo() {
      return appliesTo;
   }

   public void setAppliesTo(String appliesTo) {
      this.appliesTo = appliesTo;
   }

   public void setSubjectConfirmationId(String subjectConfirmationId) {
      this.subjectConfirmationId = subjectConfirmationId;
   }

   public String getSubjectConfirmationId() {
      return subjectConfirmationId;
   }

   public void setSubjectConfirmationRole(String subjectConfirmationRole) {
      this.subjectConfirmationRole = subjectConfirmationRole;
   }

   public String getSubjectConfirmationRole() {
      return subjectConfirmationRole;
   }

   public void setSubjectConfirmationName(String subjectConfirmationName) {
      this.subjectConfirmationName = subjectConfirmationName;
   }

   public String getSubjectConfirmationName() {
      return subjectConfirmationName;
   }

   public String getHomeCommunityId() {
      return homeCommunityId;
   }

   public void setHomeCommunityId(String homeCommunityId) {
      this.homeCommunityId = homeCommunityId;
   }

   public String getPrincipalId() { return principalId; }

   public void setPrincipalId(String principalId) { this.principalId = principalId; }

   public String getPrincipalName() { return principalName; }

   public void setPrincipalName(String principalName) { this.principalName = principalName; }
}
