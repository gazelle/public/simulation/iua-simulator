package net.ihe.gazelle.iuasimulator.xua.generator.utils;

import net.ihe.gazelle.iuasimulator.xua.generator.keystoreutils.KeyStoreManager;
import net.ihe.gazelle.iuasimulator.xua.generator.model.*;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.joda.time.DateTime;
import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.SAMLVersion;
import org.opensaml.common.SignableSAMLObject;
import org.opensaml.common.xml.SAMLConstants;
import org.opensaml.saml2.core.*;
import org.opensaml.saml2.core.impl.*;
import org.opensaml.samlext.saml2delrestrict.Delegate;
import org.opensaml.samlext.saml2delrestrict.DelegationRestrictionType;
import org.opensaml.samlext.saml2delrestrict.impl.DelegateBuilder;
import org.opensaml.samlext.saml2delrestrict.impl.DelegationRestrictionTypeBuilder;

import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.Namespace;
import org.opensaml.xml.XMLObjectBuilder;
import org.opensaml.xml.XMLObjectBuilderFactory;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.schema.XSAny;
import org.opensaml.xml.schema.impl.XSAnyBuilder;
import org.opensaml.xml.security.SecurityConfiguration;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.security.SecurityHelper;
import org.opensaml.xml.security.credential.Credential;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringWriter;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.UUID;
import java.util.logging.Logger;

public class EhealthsuisseHelperService {


   private static final Logger log = Logger.getLogger(EhealthsuisseHelperService.class.getName());
   public static final int ASSERTION_VALIDITY_DURATION_HOURS = 2;


   private EhealthsuisseHelperService() {
      super();
   }


   @SuppressWarnings("deprecation")
   public static void signSAMLAssertion(SignableSAMLObject as, KeyStoreManager keyManager, KeystoreParams keystoreParams) throws SMgrException {

      GenerateSignature result = getGenerateSignature(keyManager, keystoreParams);

      org.opensaml.xml.signature.Signature sig = (org.opensaml.xml.signature.Signature) Configuration
            .getBuilderFactory().getBuilder(org.opensaml.xml.signature.Signature.DEFAULT_ELEMENT_NAME)
            .buildObject(org.opensaml.xml.signature.Signature.DEFAULT_ELEMENT_NAME);
      Credential signingCredential = SecurityHelper.getSimpleCredential(result.cert(), result.privateKey());

      sig.setSigningCredential(signingCredential);
      sig.setSignatureAlgorithm(Constants.SIGNATURE_ALGORITHM);
      sig.setCanonicalizationAlgorithm(Constants.CANONICALIZATION_ALGORITHM);

      SecurityConfiguration secConfig = Configuration.getGlobalSecurityConfiguration();
      try {
         SecurityHelper.prepareSignatureParams(sig, signingCredential, secConfig, null);
      } catch (SecurityException e) {
         throw new SMgrException(e.getMessage(), e);
      }

      as.setSignature(sig);
      try {
         Configuration.getMarshallerFactory().getMarshaller(as).marshall(as);
      } catch (MarshallingException e) {
         throw new SMgrException(e.getMessage(), e);
      }
      try {
         org.opensaml.xml.signature.Signer.signObject(sig);
      } catch (Exception e) {
         throw new SMgrException(e.getMessage(), e);
      }
   }

   private static GenerateSignature getGenerateSignature(KeyStoreManager keyManager, KeystoreParams keystoreParams) throws SMgrException {
      X509Certificate cert;
      PrivateKey privateKey = null;
      if (keystoreParams.getKeyAlias() == null) {
         log.warning("Key alias has not been provided, generating a default certificate");
         cert = (X509Certificate) keyManager.getDefaultCertificate();
      } else {
         KeyStore keyStore;
         try {
            keyStore = KeyStore.getInstance("JKS");
         } catch (KeyStoreException e) {
            throw new SMgrException("Cannot get JKS instance of keystore", e);
         }
         File file = new File(keystoreParams.getKeystorePath());
         if (keystoreParams.getKeystorePassword() == null) {
            throw new SMgrException("keystore password shall not be null");
         } else if (keystoreParams.getPrivateKeyPassword() == null) {
            throw new SMgrException("private key password shall not be null");
         } else {
            try (FileInputStream keystoreStream = new FileInputStream(file)) {
               keyStore.load(keystoreStream, keystoreParams.getKeystorePassword().toCharArray());
               privateKey = (PrivateKey) keyStore.getKey(keystoreParams.getKeyAlias(), keystoreParams.getPrivateKeyPassword().toCharArray());
               cert = (X509Certificate) keyManager.getCertificate(keystoreParams.getKeyAlias());
            } catch (Exception e) {
               throw new SMgrException(e.getMessage(), e);
            }
         }
      }
      return new GenerateSignature(cert, privateKey);
   }

   private record GenerateSignature(X509Certificate cert, PrivateKey privateKey) {
   }


   public static AssertionImpl getUserAssertion(KeyStoreManager keystore, KeystoreParams keystoreParams,
                                                AssertionAttributes inAttributes) throws SMgrException {

      AssertionImpl assertion = EhealthsuisseHelperService.createStorkAssertion(inAttributes);
      signSAMLAssertion(assertion, keystore, keystoreParams);
      return assertion;
   }


   public static String getStringAssertion(KeyStoreManager keystore, KeystoreParams keystoreParams,
                                           AssertionAttributes inAttributes) throws MarshallingException, SMgrException {

      Element element;

      AssertionMarshaller marshaller = new AssertionMarshaller();
      AssertionImpl assertion = getUserAssertion(keystore, keystoreParams, inAttributes);
      element = marshaller.marshall(assertion);

      Document document = element.getOwnerDocument();
      StringWriter writer = new StringWriter();

      try {
         TransformerFactory factory = javax.xml.transform.TransformerFactory.newInstance();
         factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
         factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");

         Transformer transformer = factory.newTransformer();
         transformer.setOutputProperty("omit-xml-declaration", "yes");
         transformer.transform(new DOMSource(document), new StreamResult(writer));
      } catch (TransformerException e) {
         log.warning(e.getMessage());
         return null;
      }
      return writer.getBuffer().toString();

   }

   private static AssertionImpl createStorkAssertion(AssertionAttributes assertionAttributes) {

      SubjectConfirmationData subjectConfirmationData;
      ERole accessingPersonRole = ERole.getERoleByCode(assertionAttributes.getAccessingPersonRole());
      if (null == accessingPersonRole)
         accessingPersonRole = ERole.getERoleByCode("PAT");

      AssertionImpl assertion = null;
      try {
         DefaultBootstrap.bootstrap();
         XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();

         // Create the NameIdentifier
         NameIDImpl nameId = createNameIdentifier(assertionAttributes, accessingPersonRole, builderFactory);
         DateTime now = new DateTime();
         assertion = initializeAssertion(now);
         Subject subject = createSubject(nameId);
         assertion.setSubject(subject);

         // Create and add Subject Confirmation
         if (assertionAttributes.getSubjectConfirmationName() != null) {
            subjectConfirmationData = createSubjectConfirmationDataWithName(now, assertionAttributes);
         } else {
            subjectConfirmationData = createSubjectConfirmationData(now, assertionAttributes);
         }

         SubjectConfirmation subjectConf = createSubjectConfirmation(subjectConfirmationData);

         // Create and add conditions
         Conditions conditions = createConditions(now);

         AudienceRestriction audienceRestriction = createAudienceRestriction();

         conditions.getAudienceRestrictions().add(audienceRestriction);

         if (assertionAttributes.getSubjectConfirmationId() != null) {
            if (assertionAttributes.getSubjectConfirmationRole().equals("TCU")) {
               NameIDImpl tcuNameIdConf = createNameIdentifierOnBehalf(assertionAttributes, ERole.TCU, builderFactory);
               subjectConf.setNameID(tcuNameIdConf);

               NameIDImpl tcuNameIdCond = createNameIdentifierOnBehalf(assertionAttributes, ERole.TCU, builderFactory);
               DelegationRestrictionType tcuDelegate = createDelegationRestrictionType(tcuNameIdCond);
               conditions.getConditions().add(tcuDelegate);

            } else if (assertionAttributes.getSubjectConfirmationRole().equals("ASS")) {

               NameIDImpl assNameIdConf = createNameIdentifierOnBehalf(assertionAttributes, ERole.ASS, builderFactory);
               subjectConf.setNameID(assNameIdConf);

               NameIDImpl assNameIdCond = createNameIdentifierOnBehalf(assertionAttributes, ERole.ASS, builderFactory);
               DelegationRestrictionType assDelegate = createDelegationRestrictionType(assNameIdCond);
               conditions.getConditions().add(assDelegate);
            }
         }

         assertion.getSubject().getSubjectConfirmations().add(subjectConf);
         assertion.setConditions(conditions);

         Issuer issuer = createIssuer();
         assertion.setIssuer(issuer);

         // Add and create the authentication statement
         AuthnStatement authStmt = createAuthnStatement(now);
         assertion.getAuthnStatements().add(authStmt);

         // Create and add AuthnContext
         AuthnContext ac = createAuthnContext();
         authStmt.setAuthnContext(ac);

         AttributeStatement attrStmt = create(AttributeStatement.class, AttributeStatement.DEFAULT_ELEMENT_NAME);

         // XSPA Subject
         Attribute subjectAttribute = createHL7CEAttribute(builderFactory, Constants.SUBJECT_ID,
               assertionAttributes.getAccessingPersonName());
         attrStmt.getAttributes().add(subjectAttribute);

         // XSPA Role
         Attribute roleAttribute = createHL7CEAttribute(Constants.SUBJECT_ROLE, accessingPersonRole);
         attrStmt.getAttributes().add(roleAttribute);

         // XSPA Organization
         Attribute organizationAttribute = createHL7CEAttribute(builderFactory, Constants.SUBJECT_ORGANIZATION,
               assertionAttributes.getAccessingPersonOrganizationName());
         attrStmt.getAttributes().add(organizationAttribute);

         // XSPA Organization ID
         Attribute organizationIdAttribute = createHL7CEAttribute(builderFactory, Constants.SUBJECT_ORGANIZATION_ID,
               assertionAttributes.getAccessingPersonOrganizationId());
         attrStmt.getAttributes().add(organizationIdAttribute);

         // Home Community ID
         Attribute homeCommunityIdAttribute = createHL7CEAttribute(builderFactory, Constants.HOME_COMMUNITY_ID,
               assertionAttributes.getHomeCommunityId());
         attrStmt.getAttributes().add(homeCommunityIdAttribute);

         // XSPA Ressource ID
         Attribute resourceIdAttribute = createHL7CEAttribute(builderFactory, Constants.RESOURCE_RESOURCE_ID,
               assertionAttributes.getRequestedResourceId());
         attrStmt.getAttributes().add(resourceIdAttribute);

         // XSPA Purpose of Use
         EpurposeOfUse purposeOfUse = EpurposeOfUse.getEpurposeOfUseByCode(assertionAttributes.getPurposeOfUse());
         Attribute purposeOfUseAttribute = createHL7CEAttribute(Constants.SUBJECT_PURPOSEOFUSE, purposeOfUse);
         if (null != purposeOfUseAttribute)
            attrStmt.getAttributes().add(purposeOfUseAttribute);

         //Principal Id
         Attribute principalIdAttribute = createHL7CEAttribute(builderFactory, Constants.SUBJECT_PRINCIPAL_ID,
                 assertionAttributes.getPrincipalId());
         attrStmt.getAttributes().add(principalIdAttribute);

         //Principal name
         Attribute principalNameAttribute = createHL7CEAttribute(builderFactory, Constants.SUBJECT_PRINCIPAL_NAME,
                 assertionAttributes.getPrincipalName());
         attrStmt.getAttributes().add(principalNameAttribute);

         assertion.getStatements().add(attrStmt);

         assertion.addNamespace(new Namespace("http://www.w3.org/2001/XMLSchema", "xs"));
         assertion.addNamespace(new Namespace("http://www.w3.org/2001/XMLSchema-instance", "xsi"));


      } catch (ConfigurationException e) {
         log.warning(ExceptionUtils.getStackTrace(e));
      }
      return assertion;
   }

   private static AuthnContext createAuthnContext() {
      AuthnContext ac = create(AuthnContext.class, AuthnContext.DEFAULT_ELEMENT_NAME);
      AuthnContextClassRef accr = create(AuthnContextClassRef.class, AuthnContextClassRef.DEFAULT_ELEMENT_NAME);
      accr.setAuthnContextClassRef(Constants.AUTHN_CONTEXT_CLASS_REF);
      ac.setAuthnContextClassRef(accr);
      return ac;
   }

   private static AuthnStatement createAuthnStatement(DateTime now) {
      AuthnStatement authStmt = create(AuthnStatement.class, AuthnStatement.DEFAULT_ELEMENT_NAME);
      authStmt.setAuthnInstant(now);
      authStmt.setSessionNotOnOrAfter(now.plusHours(ASSERTION_VALIDITY_DURATION_HOURS));
      return authStmt;
   }

   private static Issuer createIssuer() {
      Issuer issuer = new IssuerBuilder().buildObject();
      issuer.setValue(Constants.ISSUER);
      return issuer;
   }

   private static AudienceRestriction createAudienceRestriction() {
      AudienceRestriction audienceRestriction = new AudienceRestrictionBuilder().buildObject();
      Audience issuerAudience = new AudienceBuilder().buildObject();
      issuerAudience.setAudienceURI(Constants.AUDIENCE_ALL_COMMUNITIES);
      audienceRestriction.getAudiences().add(issuerAudience);
      return audienceRestriction;
   }

   private static Conditions createConditions(DateTime now) {
      Conditions conditions = create(Conditions.class, Conditions.DEFAULT_ELEMENT_NAME);

      conditions.setNotBefore(now);
      conditions.setNotOnOrAfter(now.plusHours(ASSERTION_VALIDITY_DURATION_HOURS));
      return conditions;
   }

   private static SubjectConfirmation createSubjectConfirmation(SubjectConfirmationData subjectConfirmationData) {
      SubjectConfirmation subjectConf = create(SubjectConfirmation.class,
            SubjectConfirmation.DEFAULT_ELEMENT_NAME);

      subjectConf.setSubjectConfirmationData(subjectConfirmationData);
      subjectConf.setMethod(SubjectConfirmation.METHOD_BEARER);
      return subjectConf;
   }

   private static Subject createSubject(NameIDImpl nameId) {
      Subject subject = create(Subject.class, Subject.DEFAULT_ELEMENT_NAME);
      subject.setNameID(nameId);
      return subject;
   }

   private static AssertionImpl initializeAssertion(DateTime issueInstant) {
      AssertionImpl assertion;
      assertion = create(AssertionImpl.class, Assertion.DEFAULT_ELEMENT_NAME);
      String assId = "_" + UUID.randomUUID().toString();
      assertion.setID(assId);
      assertion.setVersion(SAMLVersion.VERSION_20);
      assertion.setIssueInstant(issueInstant);
      return assertion;
   }

   private static NameIDImpl createNameIdentifier(AssertionAttributes assertionAttributes, ERole accessingPersonRole, XMLObjectBuilderFactory
         builderFactory) {
      SAMLObjectBuilder nameIdBuilder = (SAMLObjectBuilder) builderFactory
            .getBuilder(NameID.DEFAULT_ELEMENT_NAME);
      NameIDImpl nameId = (NameIDImpl) nameIdBuilder.buildObject();
      nameId.setValue(assertionAttributes.getAccessingPersonId());
      nameId.setNameQualifier(accessingPersonRole.getNameQualifier());
      nameId.setFormat(NameIDType.PERSISTENT);
      return nameId;
   }

   private static NameIDImpl createNameIdentifierOnBehalf(AssertionAttributes assertionAttributes, ERole accessingPersonRole, XMLObjectBuilderFactory
         builderFactory) {
      SAMLObjectBuilder nameIdBuilder = (SAMLObjectBuilder) builderFactory
            .getBuilder(NameID.DEFAULT_ELEMENT_NAME);
      NameIDImpl nameId = (NameIDImpl) nameIdBuilder.buildObject();
      nameId.setValue(assertionAttributes.getSubjectConfirmationId());
      nameId.setNameQualifier(accessingPersonRole.getNameQualifier());
      nameId.setFormat(NameIDType.PERSISTENT);
      return nameId;
   }

   public static DelegationRestrictionType createDelegationRestrictionType(NameID nameID) {
      DelegationRestrictionType delegationRestrictionType = new DelegationRestrictionTypeBuilder().buildObject();
      Delegate delegate = new DelegateBuilder().buildObject();
      delegate.setNameID(nameID);
      delegationRestrictionType.getDelegates().add(delegate);
      return delegationRestrictionType;
   }


   private static Attribute createHL7CEAttribute(XMLObjectBuilderFactory builderFactory, String oasisName, String value) {

      //NOTE : These two lines make sure that the xml line gets created even though the value is not specified.
      //It got ridden off because it created empty lines that seemed weird, so if it ends up being needed please uncomment them. + get the return out of the if
      //Attribute attrPID = create(Attribute.class, Attribute.DEFAULT_ELEMENT_NAME);
      //attrPID.setName(oasisName);


      if (value != null) {
         Attribute attrPID = create(Attribute.class, Attribute.DEFAULT_ELEMENT_NAME);
         attrPID.setName(oasisName);
         XSAnyBuilder builder = (XSAnyBuilder) builderFactory.getBuilder(XSAny.TYPE_NAME);
         XSAny attrVal = builder.buildObject(AttributeValue.DEFAULT_ELEMENT_NAME);
         attrVal.setTextContent(value);
         attrPID.getAttributeValues().add(attrVal);
         return attrPID;
      }
   return null;

   }

   private static Attribute createHL7CEAttribute(String oasisName, CEDatatype name) {
      if (name != null) {
         XMLObjectBuilderFactory bf = Configuration.getBuilderFactory();
         XMLObjectBuilder<XSAny> xsAnyBuilder = bf.getBuilder(XSAny.TYPE_NAME);

         XSAny hl7CeValue = xsAnyBuilder.buildObject(Constants.V3_NAMESPACE, name.getHL7v3Name(), "");
         hl7CeValue.getUnknownAttributes().put(new QName("http://www.w3.org/2001/XMLSchema-instance", "type", "xsi"), "CE");
         hl7CeValue.getUnknownAttributes().put(new QName(Constants.CODE), name.getCode());
         hl7CeValue.getUnknownAttributes().put(new QName(Constants.CODE_SYSTEM), name.getCodeSystemName());
         hl7CeValue.getUnknownAttributes().put(new QName(Constants.CODE_SYSTEM_NAME), name.getCodeSystem());
         hl7CeValue.getUnknownAttributes().put(new QName(Constants.DISPLAY_NAME), name.getDisplayName());

         XSAny attributeValue = xsAnyBuilder.buildObject(AttributeValue.DEFAULT_ELEMENT_NAME);
         attributeValue.getUnknownXMLObjects().add(hl7CeValue);

         Attribute attribute = create(Attribute.class, Attribute.DEFAULT_ELEMENT_NAME);
         attribute.setName(oasisName);
         attribute.getAttributeValues().add(attributeValue);
         return attribute;
      }
      return null;
   }


   private static <T> T create(Class<T> cls, QName qname) {
      return (T) (Configuration.getBuilderFactory().getBuilder(qname)).buildObject(qname);
   }

   private static SubjectConfirmationData createSubjectConfirmationData(DateTime now, AssertionAttributes assertionAttributes) {
      SubjectConfirmationData subjectConfirmationData = create(SubjectConfirmationData.class,
            SubjectConfirmationData.DEFAULT_ELEMENT_NAME);
      subjectConfirmationData.setRecipient(assertionAttributes.getAppliesTo());
      subjectConfirmationData.setInResponseTo("_43ef3e4fefdb03a31781d7ea52617674bcd25a6c26");
      subjectConfirmationData.setNotOnOrAfter(now.plusHours(ASSERTION_VALIDITY_DURATION_HOURS));
      return subjectConfirmationData;
   }

   private static SubjectConfirmationData createSubjectConfirmationDataWithName(DateTime now, AssertionAttributes assertionAttributes) {
      SubjectConfirmationDataWithName subjectConfirmationData = new SubjectConfirmationDataWithName(SAMLConstants.SAML20_NS,
            "SubjectConfirmationData",
            SAMLConstants.SAML20_PREFIX);
      subjectConfirmationData.setRecipient(assertionAttributes.getAppliesTo());
      subjectConfirmationData.setInResponseTo("_43ef3e4fefdb03a31781d7ea52617674bcd25a6c26");
      subjectConfirmationData.setNotOnOrAfter(now.plusHours(ASSERTION_VALIDITY_DURATION_HOURS));


      AttributeStatement attrStmt = create(AttributeStatement.class, AttributeStatement.DEFAULT_ELEMENT_NAME);
      XMLObjectBuilderFactory builderFactory = Configuration.getBuilderFactory();

      Attribute attributeName = createHL7CEAttribute(builderFactory, Constants.SUBJECT_ID,
            assertionAttributes.getSubjectConfirmationName());
      attrStmt.getAttributes().add(attributeName);

      subjectConfirmationData.setAttributeStatementName(attrStmt);
      return subjectConfirmationData;
   }


}
