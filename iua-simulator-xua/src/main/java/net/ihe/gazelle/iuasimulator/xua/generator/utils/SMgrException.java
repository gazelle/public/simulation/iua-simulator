package net.ihe.gazelle.iuasimulator.xua.generator.utils;



import javax.xml.crypto.dsig.XMLSignatureException;

public class SMgrException extends XMLSignatureException {

    public SMgrException(String exceptionMessage, Exception e) {
        super(exceptionMessage, e);
    }

    public SMgrException(String exceptionMessage) {
        super(exceptionMessage);
    }

}
