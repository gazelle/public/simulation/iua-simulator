package net.ihe.gazelle.iuasimulator.xua.generator.model;

/**
 * <p>ERole enum.</p>
 *
 * @author aberge
 * @version 1.0: 26/09/17
 */
public enum ERole implements CEDatatype {

    PAT("PAT", "Patient", "urn:e-health-suisse:2015:epr-spid"),
    HCP("HCP", "Gesundheitsfachperson", "urn:gs1:gln"),
    ASS("ASS", "Hilfsperson", "urn:gs1:gln"),
    REP("REP", "Stellvertretung", "urn:e-health-suisse:representative-id"),
    PADM("PADM", "Datenschutzrichtlinien-Administrator", "urn:e-health-suisse:policy-administrator-id"),
    DADM("DADM", "Dokumentenadministrator", "urn:e-health-suisse:document-administrator-id"),
    TCU("TCU", "Technischer Nutzer", "urn:e-health-suisse:technical-user-id");

    String code;
    String displayName;
    String nameQualifier;

    ERole(String code, String displayName, String nameQualifier) {
        this.code = code;
        this.displayName = displayName;
        this.nameQualifier = nameQualifier;
    }

    public static ERole getERoleByCode(String inCode) {
        for (ERole value : values()) {
            if (value.getCode().equals(inCode)) {
                return value;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getNameQualifier() {
        return nameQualifier;
    }

    public String getHL7v3Name() {
        return "Role";
    }

    public String getCodeSystem() {
        return "eHealth Suisse EPR Akteure";
    }

    public String getCodeSystemName() {
        return "2.16.756.5.30.1.127.3.10.6";
    }
}
