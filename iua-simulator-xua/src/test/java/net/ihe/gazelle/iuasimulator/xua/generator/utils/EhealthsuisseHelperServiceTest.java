package net.ihe.gazelle.iuasimulator.xua.generator.utils;

import net.ihe.gazelle.iuasimulator.xua.generator.model.AssertionAttributes;
import net.ihe.gazelle.iuasimulator.xua.generator.model.KeystoreParams;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EhealthsuisseHelperServiceTest {

    private static final String DEFAULT_KEYSTORE_PATH = "cert/iua.jks";
    private static final String DEFAULT_KEYSTORE_PASSWORD = "password";
    private static final String DEFAULT_KEY_ALIAS = ("ch-iua-client");
    private static final String DEFAULT_KEY_PASSWORD = "password";
    private static final String DEFAULT_TRUSTSTORE_PATH = DEFAULT_KEYSTORE_PATH;
    private static final String DEFAULT_TRUSTSTORE_PASSWORD = DEFAULT_KEYSTORE_PASSWORD;

    @Test
    void AssTest() {
        try {
            KeystoreParams keystoreParams = new KeystoreParams(DEFAULT_KEYSTORE_PATH, DEFAULT_KEYSTORE_PASSWORD, DEFAULT_TRUSTSTORE_PATH, DEFAULT_TRUSTSTORE_PASSWORD, DEFAULT_KEY_ALIAS, DEFAULT_KEY_PASSWORD);
            AssertionAttributes assertionAttributes = new AssertionAttributes("personId", "personId", "EMER", "personId", "groupId", "group", "HCP", "confirmationId", "ASS", "confirmationName", "groupId", "principalId", "principal");
            String assertion = AssertionUtils.getStringAssertionWithPath(assertionAttributes, keystoreParams);
            Assertions.assertNotNull(assertion);
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }

    @Test
    void TcuTest() {
        try {
            KeystoreParams keystoreParams = new KeystoreParams(DEFAULT_KEYSTORE_PATH, DEFAULT_KEYSTORE_PASSWORD, DEFAULT_TRUSTSTORE_PATH, DEFAULT_TRUSTSTORE_PASSWORD, DEFAULT_KEY_ALIAS, DEFAULT_KEY_PASSWORD);
            AssertionAttributes assertionAttributes = new AssertionAttributes("personId", "personId", "EMER", "personId", "groupId", "group", "HCP", "confirmationId", "TCU", null, "groupId", "principalId", "principal");
            String assertion = AssertionUtils.getStringAssertionWithPath(assertionAttributes, keystoreParams);
            Assertions.assertNotNull(assertion);
        } catch (Exception e) {
            Assertions.fail(e);
        }
    }

}
