package net.ihe.gazelle.iuasimulator.interlay.resource.validator;

import net.ihe.gazelle.iuasimulator.application.utils.RegexUtils;
import net.ihe.gazelle.iuasimulator.domain.exception.ValidatorException;
import net.ihe.gazelle.iuasimulator.domain.model.IheTokenParamBuilder;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.IheTokenParamValidationImpl;
import net.ihe.gazelle.iuasimulator.interlay.utils.RegexUtilsImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 17/04/2023
 */
class IheTokenQueryParamValidatorImplTest {

    private final RegexUtils regexUtils = new RegexUtilsImpl();
    private final IheTokenParamValidationImpl tokenParam = new IheTokenParamValidationImpl();
    private final IheTokenQueryParamValidatorImpl validator = new IheTokenQueryParamValidatorImpl(tokenParam, regexUtils);
    private static String url;

    @BeforeAll
    static void setup() {
        url = "http://localhost:8080/iua-simulator/rest/ihe/callback";
    }

    @Test
    void validateTokenPostRequest_ko_grantType_blank() {
        IheTokenParamBuilder param = IheTokenParamBuilder.builder()
                .withCode("9638d465-4c98-4429-93f2-bd8c92002d8d.1f8d2c3d-1c68-4fb9-9bb3-7fc22c174883.3d59b3b0-1ede-4eda-8f3a-a2fd22520a9b")
                .withCodeVerifier("--5Q635ku8ER2pfGeuDagpsk40~5kdbhPftnIaZubTAHKLXW5ZZQA9~HXOrSGOlXn52fbrVnc0JCUMmAFF0TOIaGOqRNjsiA2o0Y3m3_61~JsfYvR_kLyAzInMCd_aPF")
                .withRedirectUri("http://localhost:8080/iua-simulator/rest/ihe/callback")
                .withGrantType("").build();

        assertThrows(ValidatorException.class, () -> validator.validateTokenRequest(param));
    }

    @Test
    void validateTokenPostRequest_ko_code_blank() {
        IheTokenParamBuilder param = IheTokenParamBuilder.builder()
                .withCode("")
                .withCodeVerifier("--5Q635ku8ER2pfGeuDagpsk40~5kdbhPftnIaZubTAHKLXW5ZZQA9~HXOrSGOlXn52fbrVnc0JCUMmAFF0TOIaGOqRNjsiA2o0Y3m3_61~JsfYvR_kLyAzInMCd_aPF")
                .withRedirectUri("http://localhost:8080/iua-simulator/rest/ihe/callback")
                .withGrantType("authorization_code").build();
        assertThrows(ValidatorException.class, () -> validator.validateTokenRequest(param));
    }

    @Test
    void validateTokenPostRequest_ko_codeVerifier_blank() {
        IheTokenParamBuilder param = IheTokenParamBuilder.builder()
                .withCode("9638d465-4c98-4429-93f2-bd8c92002d8d.1f8d2c3d-1c68-4fb9-9bb3-7fc22c174883.3d59b3b0-1ede-4eda-8f3a-a2fd22520a9b")
                .withCodeVerifier("")
                .withRedirectUri("http://localhost:8080/iua-simulator/rest/ihe/callback")
                .withGrantType("authorization_code").build();
        assertThrows(ValidatorException.class, () -> validator.validateTokenRequest(param));
    }

    @Test
    void validateTokenPostRequest_ko_redirectUri_blank() {
        IheTokenParamBuilder param = IheTokenParamBuilder.builder()
                .withCode("9638d465-4c98-4429-93f2-bd8c92002d8d.1f8d2c3d-1c68-4fb9-9bb3-7fc22c174883.3d59b3b0-1ede-4eda-8f3a-a2fd22520a9b")
                .withCodeVerifier("--5Q635ku8ER2pfGeuDagpsk40~5kdbhPftnIaZubTAHKLXW5ZZQA9~HXOrSGOlXn52fbrVnc0JCUMmAFF0TOIaGOqRNjsiA2o0Y3m3_61~JsfYvR_kLyAzInMCd_aPF")
                .withRedirectUri("")
                .withGrantType("authorization_code").build();
        assertThrows(ValidatorException.class, () -> validator.validateTokenRequest(param));
    }
}