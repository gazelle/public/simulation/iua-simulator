package net.ihe.gazelle.iuasimulator.interlay.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import net.ihe.gazelle.iuasimulator.domain.model.IheScopeParamBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.opensaml.xml.util.Pair;

import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.any;


import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 13/04/2023
 */
class IheScopeMapperTest {

    private final IheScopeMapper mapper = new IheScopeMapper();

    private final JsonObject jsonGenerationTest =Json.createObjectBuilder()
            .add("id", "bfc7ed20-8efe-43bd-ae72-72cfa7fd934")
                .add("name", "ITI-63")
                .add("description", "ITI-63" + " scope")
                .add("protocol","openid-connect")
                .add("attributes", Json.createObjectBuilder()
                        .add("include.in.token.scope","true")
                        .add("display.on.consent.screen", "false")
                        .add("gui.order","")
                        .add("consent.screen.text","")).build();

    /*@Test
    void getScopeFromQueryParamTest() {
        Map<String, String> mapTest = new HashMap<>();
        mapTest.put("abcde","abcde");
        mapTest.put("fgh","ijk");
        Mockito.when(om.convertValue(any(), eq(Map.class))).thenReturn(mapTest);
        String scopeToTest = "abcde fgh=ijk";
        IheScopeParamBuilder scopeParam = mapper.getScopeFromQueryParam(scopeToTest);

        Assertions.assertNotNull(scopeParam);

        Map<String, String> returnedMap = mapper.getScopeMap(scopeParam);

        Assertions.assertNotNull(returnedMap);
    }*/

    @Test
    void scopeClientGeneration() {
        String scopteToGenerate = "ITI-63";
        Pair<String, JsonObject> pairToTest =  mapper.scopeClientGeneration(scopteToGenerate);
        Assertions.assertEquals("bfc7ed20-8efe-43bd-ae72-72cfa7fd934",pairToTest.getFirst());
        Assertions.assertEquals(jsonGenerationTest, pairToTest.getSecond());
    }


}