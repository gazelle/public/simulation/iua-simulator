package net.ihe.gazelle.iuasimulator.interlay.resource.validator;

import net.ihe.gazelle.iuasimulator.application.param.IuaAuthQueryValidator;
import net.ihe.gazelle.iuasimulator.application.utils.RegexUtils;
import net.ihe.gazelle.iuasimulator.domain.exception.ValidatorException;
import net.ihe.gazelle.iuasimulator.domain.model.IheAuthParamBuilder;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.IheAuthParamValidationImpl;
import net.ihe.gazelle.iuasimulator.interlay.utils.RegexUtilsImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 17/04/2023
 */
class IheCodeQueryParamValidatorImplTest {

    private final RegexUtils factoryUtils = new RegexUtilsImpl();
    private final IheAuthParamValidationImpl authParam = new IheAuthParamValidationImpl();
    private final IuaAuthQueryValidator<IheAuthParamBuilder> validator = new IheAuthQueryParamValidatorImpl(authParam, factoryUtils);
    private String clientName;
    private String url;

    @BeforeEach
    void setup(){
        clientName = "clientName";
        url = "http://localhost/iua-simulator/rest/ihe/callback";
    }

    @Test
    void validateCodeAuthorization_ko_responseType_null() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder().build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));

    }
    @Test
    void validateCodeAuthorization_ko_responseType_format() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost/iua-simulator/rest/ihe/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("bad").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_responseType_blank() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost/iua-simulator/rest/ihe/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_codeChallenge_format() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost/iua-simulator/rest/ihe/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_codeChallenge_blank() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder()
                .withCodeChallenge("")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost/iua-simulator/rest/ihe/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_state_blank() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("")
                .withScope("launch")
                .withRedirectUri("http://localhost/iua-simulator/rest/ihe/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_clientId_blank() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost/iua-simulator/rest/ihe/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_codeChallengeMethod_check() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost/iua-simulator/rest/ihe/callback")
                .withCodeChallengeMethod("SH256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_codeChallengeMethod_blank() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://localhost/iua-simulator/rest/ihe/callback")
                .withCodeChallengeMethod("")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_redirectUri_format() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("localhost:8090/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_redirectUri_blank() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("https://pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_audience_format() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://context/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("")
                .withClientId("clientName")
                .withAudience("pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_audience_blank() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("launch")
                .withRedirectUri("http://context/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }

    @Test
    void validateCodeAuthorization_ko_scope_blank() {
        IheAuthParamBuilder chCodeParam = IheAuthParamBuilder.builder()
                .withCodeChallenge("fDH8WRQ-vp-6dU6_llO2Fa7wHYSoiKOWoDNxl_u0TLg")
                .withState("oui")
                .withScope("")
                .withRedirectUri("http://context/iua-simulator/rest/ch/callback")
                .withCodeChallengeMethod("S256")
                .withClientId("clientName")
                .withAudience("pixmResourceServerURL.ch")
                .withResponseType("code").build();
        assertThrows(ValidatorException.class, () -> validator.validateCodeAuthorization(chCodeParam));
    }
}