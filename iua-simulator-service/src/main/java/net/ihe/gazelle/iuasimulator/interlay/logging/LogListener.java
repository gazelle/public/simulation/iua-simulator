package net.ihe.gazelle.iuasimulator.interlay.logging;

import jakarta.inject.Inject;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;

import java.lang.reflect.Method;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 06/02/2023
 */
@net.ihe.gazelle.iuasimulator.interlay.logging.LogAudited
@Interceptor
public class LogListener {

    @Inject
    LogService logger;

    @AroundInvoke
    public Object audit(InvocationContext context) throws Exception {
        Method method = context.getMethod();
        Class<?> clazz = method.getDeclaringClass();

        // check if annotation is on class or method
        LogLevel logLevel = method.getAnnotation(net.ihe.gazelle.iuasimulator.interlay.logging.LogAudited.class)!= null ?
                method.getAnnotation(net.ihe.gazelle.iuasimulator.interlay.logging.LogAudited.class).value() :
                clazz.getAnnotation(LogAudited.class).value();

        String logMessage = String.format("Calling method %s in class %s", method.getName(), clazz.getName());
        logger.log(method.getDeclaringClass().getCanonicalName(), logLevel, logMessage);

        return context.proceed();
    }
}
