package net.ihe.gazelle.iuasimulator.interlay.resource.validator;

import jakarta.inject.Inject;
import net.ihe.gazelle.iuasimulator.application.param.IuaTokenQueryValidator;
import net.ihe.gazelle.iuasimulator.application.utils.RegexUtils;
import net.ihe.gazelle.iuasimulator.domain.exception.ValidatorException;
import net.ihe.gazelle.iuasimulator.domain.model.IheTokenParamBuilder;
import net.ihe.gazelle.iuasimulator.interlay.logging.LogAudited;
import net.ihe.gazelle.iuasimulator.interlay.logging.LogLevel;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.IheTokenParamValidationImpl;

import static java.util.Objects.isNull;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 12/04/2023
 */
@LogAudited(LogLevel.DEBUG)
public class IheTokenQueryParamValidatorImpl implements IuaTokenQueryValidator<IheTokenParamBuilder> {

    private final RegexUtils regexUtils;
    private final IheTokenParamValidationImpl tokenParam;

    @Inject
    public IheTokenQueryParamValidatorImpl(IheTokenParamValidationImpl tokenParam, RegexUtils regexUtils) {
        this.regexUtils = regexUtils;
        this.tokenParam = tokenParam;
    }

    public void validateTokenRequest(final IheTokenParamBuilder iheTokenParam) {
        if (isNull(iheTokenParam.getGrandType()) || iheTokenParam.getGrandType().isBlank())
            throw new ValidatorException(tokenParam.getGrantTypeRequired());
        if (isNull(iheTokenParam.getCode()) || iheTokenParam.getCode().isBlank())
            throw new ValidatorException(tokenParam.getCodeRequired());
        if (isNull(iheTokenParam.getCodeVerifier()) || iheTokenParam.getCodeVerifier().isBlank())
            throw new ValidatorException(tokenParam.getCodeVerifierRequired());
        if (isNull(iheTokenParam.getRedirectUri()) || iheTokenParam.getRedirectUri().isBlank())
            throw new ValidatorException(tokenParam.getRedirectUriRequired());

        if (!regexUtils.isValidURL(iheTokenParam.getRedirectUri()))
            throw new ValidatorException(tokenParam.getUrlFormat());
    }
}