package net.ihe.gazelle.iuasimulator.interlay.utils;

import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;

public class JsonScopeUtils {

    public static final String FALSE = "false";
    private JsonScopeUtils() { }

    public static JsonObjectBuilder createJsonScope(String scope, String id){
        return Json.createObjectBuilder()
                .add("id", id)
                .add("name", scope)
                .add("description", scope + " scope")
                .add("protocol","openid-connect")
                .add("attributes", Json.createObjectBuilder()
                        .add("include.in.token.scope","true")
                        .add("display.on.consent.screen", FALSE)
                        .add("gui.order","")
                        .add("consent.screen.text",""));
    }

    public static JsonObject getScopeJson(String scope, String id) {
        JsonObject scopeToCreate;
        JsonObjectBuilder scopeJson = JsonScopeUtils.createJsonScope(scope, id);
        scopeToCreate = scopeJson.build();
        return scopeToCreate;
    }

}
