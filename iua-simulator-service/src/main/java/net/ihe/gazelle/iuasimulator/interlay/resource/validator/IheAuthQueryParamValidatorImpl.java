package net.ihe.gazelle.iuasimulator.interlay.resource.validator;

import jakarta.inject.Inject;
import net.ihe.gazelle.iuasimulator.application.param.IuaAuthQueryValidator;
import net.ihe.gazelle.iuasimulator.application.utils.RegexUtils;
import net.ihe.gazelle.iuasimulator.domain.exception.ValidatorException;
import net.ihe.gazelle.iuasimulator.domain.model.IheAuthParamBuilder;
import net.ihe.gazelle.iuasimulator.interlay.logging.LogAudited;
import net.ihe.gazelle.iuasimulator.interlay.logging.LogLevel;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.IheAuthParamValidationImpl;

import static java.util.Objects.isNull;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 12/04/2023
 */
@LogAudited(LogLevel.DEBUG)
public class IheAuthQueryParamValidatorImpl implements IuaAuthQueryValidator<IheAuthParamBuilder> {


    private final IheAuthParamValidationImpl authParam;
    private final RegexUtils regexUtils;

    @Inject
    public IheAuthQueryParamValidatorImpl(IheAuthParamValidationImpl authParam, RegexUtils regexUtils) {
        this.regexUtils = regexUtils;
        this.authParam = authParam;
    }

    @Override
    public void validateCodeAuthorization(final IheAuthParamBuilder iheCodeParam) {
        validateNonEmptyness(iheCodeParam);
        validateValues(iheCodeParam);
    }

    private void validateValues(IheAuthParamBuilder iheCodeParam) {
        if (!regexUtils.isValidURL(iheCodeParam.getRedirectUri()))
            throw new ValidatorException(authParam.getUrlFormat());
        if (!isNull(iheCodeParam.getAudience()) && !regexUtils.isValidURL(iheCodeParam.getAudience()))
            throw new ValidatorException(authParam.getUrlFormat());
        if (!regexUtils.isValidCodeChallengeMethodValue(iheCodeParam.getCodeChallengeMethod()))
            throw new ValidatorException(authParam.getBadCodeChallengeMethod());
        if (!regexUtils.isValidResponseTypeValue(iheCodeParam.getResponseType()))
            throw new ValidatorException(authParam.getBadResponseType());
        if (!regexUtils.isValidCodeChallengeFormat(iheCodeParam.getCodeChallenge()))
            throw new ValidatorException(authParam.getBadCodeChallengeFormat());
    }

    private void validateNonEmptyness(IheAuthParamBuilder iheCodeParam) {
        if (isNull(iheCodeParam.getCodeChallenge()) || iheCodeParam.getCodeChallenge().isBlank())
            throw new ValidatorException(authParam.getCodeChallengeRequired());
        if (isNull(iheCodeParam.getCodeChallengeMethod()) || iheCodeParam.getCodeChallengeMethod().isBlank())
            throw new ValidatorException(authParam.getCodeChallengeMethodRequired());
        if (isNull(iheCodeParam.getResponseType()) || iheCodeParam.getResponseType().isBlank())
            throw new ValidatorException(authParam.getResponseTypeRequired());
        if (isNull(iheCodeParam.getClientId()) || iheCodeParam.getClientId().isBlank())
            throw new ValidatorException(authParam.getClientIdRequired());
        if (isNull(iheCodeParam.getRedirectUri()) || iheCodeParam.getRedirectUri().isBlank())
            throw new ValidatorException(authParam.getRedirectUriRequired());
        if (isNull(iheCodeParam.getState()) || iheCodeParam.getState().isBlank())
            throw new ValidatorException(authParam.getStateRequired());
    }

}