package net.ihe.gazelle.iuasimulator.interlay.logging;

import jakarta.enterprise.util.Nonbinding;
import jakarta.interceptor.InterceptorBinding;

import java.lang.annotation.*;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 06/02/2023
 */
@Inherited
@InterceptorBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface LogAudited {
    @Nonbinding
    net.ihe.gazelle.iuasimulator.interlay.logging.LogLevel value() default LogLevel.DEBUG;
}
