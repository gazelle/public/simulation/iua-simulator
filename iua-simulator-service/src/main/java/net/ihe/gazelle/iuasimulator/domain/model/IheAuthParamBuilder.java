package net.ihe.gazelle.iuasimulator.domain.model;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 09/04/2023
 */
public class IheAuthParamBuilder extends IuaAuthParamBuilder {

    protected IheAuthParamBuilder(IheBuilder builder) {
        super(builder);
    }

    public static IheBuilder builder() { return new IheBuilder();}

    public static class IheBuilder extends IuaBuilder<IheBuilder> {
        @Override
        public IheBuilder getThis() { return this;}

        @Override
        public IheAuthParamBuilder build() { return new IheAuthParamBuilder(this);}
    }
}
