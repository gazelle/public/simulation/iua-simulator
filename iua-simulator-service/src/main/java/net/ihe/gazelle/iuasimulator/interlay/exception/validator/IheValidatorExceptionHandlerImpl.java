package net.ihe.gazelle.iuasimulator.interlay.exception.validator;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import net.ihe.gazelle.iuasimulator.domain.exception.ValidatorErrorMessage;
import net.ihe.gazelle.iuasimulator.domain.exception.ValidatorException;
import net.ihe.gazelle.iuasimulator.interlay.logging.LogAudited;
import net.ihe.gazelle.iuasimulator.interlay.logging.LogLevel;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.IheAuthParamValidationImpl;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.param.IheTokenParamValidationImpl;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 12/04/2023
 */
@Provider
@LogAudited(LogLevel.DEBUG)
public class IheValidatorExceptionHandlerImpl implements ExceptionMapper<ValidatorException> {

    @Inject
    private IheAuthParamValidationImpl authParam;
    @Inject
    private IheTokenParamValidationImpl tokenParam;

    @Override
    public Response toResponse(ValidatorException e) {
        if (codeAuthValidation(e)) return getResponse(e);
        if (tokenPostValidation(e)) return getResponse(e);
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ValidatorErrorMessage("Unexpected error", false)).build();
    }


    private boolean codeAuthValidation(ValidatorException e) {
        if (e.getMessage().equalsIgnoreCase(authParam.getResponseTypeRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getClientIdRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getBadClientName())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getStateRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getCodeChallengeRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getRedirectUriRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getCodeChallengeMethodRequired())) return true;

        if (e.getMessage().equalsIgnoreCase(authParam.getUrlFormat())) return true;
        if (e.getMessage().equalsIgnoreCase(authParam.getBadClientName())) return true;
        return (e.getMessage().equalsIgnoreCase(authParam.getRedirectUriValue()));
    }

    private boolean tokenPostValidation(ValidatorException e) {
        if (e.getMessage().equalsIgnoreCase(tokenParam.getGrantTypeRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(tokenParam.getCodeRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(tokenParam.getCodeVerifierRequired())) return true;
        if (e.getMessage().equalsIgnoreCase(tokenParam.getRedirectUriRequired())) return true;

        if (e.getMessage().equalsIgnoreCase(tokenParam.getUrlFormat())) return true;
        return (e.getMessage().equalsIgnoreCase(tokenParam.getRedirectUriValue()));
    }


    private Response getResponse(ValidatorException e) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ValidatorErrorMessage(e.getMessage(), false)).build();
    }
}
