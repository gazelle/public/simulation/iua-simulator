package net.ihe.gazelle.iuasimulator.interlay.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.json.JsonObject;
import net.ihe.gazelle.iuasimulator.application.mapper.IuaScopeMapper;
import net.ihe.gazelle.iuasimulator.domain.model.IheScopeParamBuilder;
import net.ihe.gazelle.iuasimulator.interlay.utils.JsonScopeUtils;
import org.opensaml.xml.util.Pair;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 10/04/2023
 */
public class IheScopeMapper implements IuaScopeMapper<IheScopeParamBuilder> {


    @Override
    public IheScopeParamBuilder getScopeFromQueryParam(String scope) {
        IheScopeParamBuilder.IheBuilder scopeParam = new IheScopeParamBuilder.IheBuilder();
        for (String s : scope.split(" ")) {
            if (s.contains("=")) {
                String[] scopeValues = s.split("=");
                checkSplitScope(scopeParam, scopeValues);
            } else {
                String[] scopeValues = new String[]{s};
                checkSplitScope(scopeParam, scopeValues);
            }
        }
        return scopeParam.build();
    }

    private static void checkSplitScope(IheScopeParamBuilder.IheBuilder scopeParam, String[] scopeValues) {
        //Scope are not yet determined in the IHE specification.
    }



    @Override
    public Map<String, String> getScopeMap(IheScopeParamBuilder param) {
        ObjectMapper om = new ObjectMapper();
        Map<String, String> test = om.convertValue(param, Map.class);
        Map<String, String> returnedMap = new HashMap<>();
        for (Map.Entry<String, String> entry : test.entrySet()) {
            returnedMap.put(toUnderscore(entry.getKey()), entry.getValue());
        }
        return returnedMap;
    }

    private String toUnderscore(String value) {
        String regex = "([a-z])([A-Z])";
        String replacement = "$1_$2";
        return value.replaceAll(regex, replacement).toLowerCase();
    }

    public Pair<String, JsonObject> scopeClientGeneration(String scope) {
        JsonObject scopeToCreate;
        String id = "bfc7ed20-8efe-43bd-ae72-72cfa7fd934";
        scopeToCreate = JsonScopeUtils.getScopeJson(scope, id);
        return new Pair<>(id, scopeToCreate);
    }
}