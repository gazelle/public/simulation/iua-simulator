package net.ihe.gazelle.iuasimulator.interlay.resource;

import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import net.ihe.gazelle.iuasimulator.application.resource.IuaResource;
import net.ihe.gazelle.iuasimulator.client.interlay.keycloak.KeycloakClient;
import net.ihe.gazelle.iuasimulator.domain.model.IheAuthParamBuilder;
import net.ihe.gazelle.iuasimulator.domain.model.IheTokenParamBuilder;
import net.ihe.gazelle.iuasimulator.interlay.logging.LogAudited;
import net.ihe.gazelle.iuasimulator.interlay.logging.LogLevel;
import net.ihe.gazelle.iuasimulator.interlay.mapper.IheScopeMapper;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.IheAuthQueryParamValidatorImpl;
import net.ihe.gazelle.iuasimulator.interlay.resource.validator.IheTokenQueryParamValidatorImpl;
import net.ihe.gazelle.iuasimulator.interlay.utils.IuaJsonMetadataUtils;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 02/02/2023
 */
@LogAudited(LogLevel.INFO)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("ihe")
public class IheResourceImpl implements IuaResource<Response> {

    @Inject
    private IheAuthQueryParamValidatorImpl codeParamValidator;
    @Inject
    private IheTokenQueryParamValidatorImpl tokenParamValidator;
    private static final String ENDPOINT_KEY = System.getenv("REALMS_CONF_IHE_IUA_JWT_ENDPOINT");
    private KeycloakClient client;
    private final IuaJsonMetadataUtils metadataUtils = new IuaJsonMetadataUtils("ihe");

    private IheScopeMapper scopeMapper = new IheScopeMapper();

    @GET
    @Path("authorize")
    @Override
    public Response getAuthCode(@QueryParam("response_type") String responseType,
                                @QueryParam("client_id") String clientId,
                                @QueryParam("state") String state,
                                @QueryParam("resource") String resource,
                                @QueryParam("code_challenge") String codeChallenge,
                                @QueryParam("code_challenge_method") String codeChallengeMethod,
                                @QueryParam("redirect_uri") String redirectUri,
                                @QueryParam("scope") String scope,
                                @QueryParam("aud") String audience,
                                @QueryParam("launch") String launch) {

        IheAuthParamBuilder iheCodeParam = IheAuthParamBuilder.builder().
                withResponseType(responseType)
                .withClientId(clientId)
                .withState(state)
                .withResource(resource)
                .withCodeChallenge(codeChallenge)
                .withCodeChallengeMethod(codeChallengeMethod)
                .withRedirectUri(redirectUri)
                .withScope(scope)
                .withAudience(audience)
                .build();

        client = new KeycloakClient(ENDPOINT_KEY);

        codeParamValidator.validateCodeAuthorization(iheCodeParam);

        try {
            return client.requestAuthCodeEndpoint(responseType, clientId, state, resource, codeChallenge, codeChallengeMethod, redirectUri, scope);
        } catch (Exception e) {
            return Response.status(Response.Status.NO_CONTENT).header("Access-Control-Allow-Origin", "*").tag("Exception Occurred").build();
        }
    }

    @GET
    @Path(".well-known")
    @Override
    public Response getMetadata() {
        String endpoint = System.getenv("IUA_URL");
        try {
            JsonObject json = metadataUtils.generateMetadata(endpoint);
            return Response.accepted().header("Access-Control-Allow-Origin", "*").entity(json).build();
        } catch (Exception e) {
            return Response.noContent().header("Access-Control-Allow-Origin", "*").build();
        }
    }

    @GET
    @Path("certs")
    public Response getCerts() {
        client = new KeycloakClient(ENDPOINT_KEY);
        try {
            return client.requestServerCerts();
        } catch (Exception e) {
            return Response.noContent().header("Access-Control-Allow-Origin", "*").build();
        }
    }

    @GET
    @Path("callback")
    @Override
    public Response retrieveCode(@QueryParam("state") String state, @QueryParam("session_state") String sessionState, @QueryParam("code") String code, @QueryParam("error_code") String errorCode, @QueryParam("error_message") String message) {
        JsonObject json;
        if (null != code) {
            json = Json.createObjectBuilder().add("code", code).build();
            return Response.accepted().header("Access-Control-Allow-Origin", "*").entity(json).build();
        } else if (null != errorCode) {
            json = Json.createObjectBuilder().add("error_message", message).build();
            return Response.status(Integer.parseInt(errorCode)).header("Access-Control-Allow-Origin", "*").entity(json).build();
        } else {
            return Response.noContent().header("Access-Control-Allow-Origin", "*").build();
        }
    }


    @POST
    @Path("token")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Override
    public Response getToken(@HeaderParam("Authorization") String headerAuth,
                             @FormParam("redirect_uri") String redirectUri,
                             @FormParam("grant_type") String grantType,
                             @FormParam("code_verifier") String codeVerifier,
                             @FormParam("code") String code,
                             @FormParam("client_session_state") String clientSessionState) {
        IheTokenParamBuilder tokenParam = IheTokenParamBuilder.builder()
                .withGrantType(grantType)
                .withRedirectUri(redirectUri)
                .withCodeVerifier(codeVerifier)
                .withCode(code)
                .build();

        Response response;
        tokenParamValidator.validateTokenRequest(tokenParam);
        try {
            client = new KeycloakClient(ENDPOINT_KEY);
            response = client.requestTokenEndpoint(headerAuth, redirectUri, grantType, codeVerifier, code, clientSessionState);
        } catch (Exception e) {
            return Response.status(Response.Status.NO_CONTENT).header("Access-Control-Allow-Origin", "*").build();
        }

        return response;
    }


    @GET
    @Path("health")
    @Override
    public Response getHealth() {
        return Response.ok().header("Access-Control-Allow-Origin", "*").build();
    }
}