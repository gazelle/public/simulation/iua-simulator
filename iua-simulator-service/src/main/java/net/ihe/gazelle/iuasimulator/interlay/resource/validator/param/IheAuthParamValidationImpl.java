package net.ihe.gazelle.iuasimulator.interlay.resource.validator.param;

import jakarta.inject.Inject;
import net.ihe.gazelle.iuasimulator.application.validation.IuaAuthParamValidation;
import net.ihe.gazelle.iuasimulator.domain.validation.IuaAuthParamValidationImpl;
import org.eclipse.microprofile.config.inject.ConfigProperty;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 13/04/2023
 */
public class IheAuthParamValidationImpl extends IuaAuthParamValidationImpl implements IuaAuthParamValidation {

    @Inject
    @ConfigProperty(name = "iua.code.auth.validator.exception.urlFormat")
    private String urlFormat;
    @Inject
    @ConfigProperty(name = "iua.code.auth.validator.exception.redirectUriValue")
    private String redirectUriValue;
    @Inject
    @ConfigProperty(name = "iua.code.auth.validator.exception.badClientName")
    private String badClientName;
    @Inject
    @ConfigProperty(name = "iua.code.auth.validator.exception.responseType")
    private String responseTypeRequired;
    @Inject
    @ConfigProperty(name = "iua.code.auth.validator.exception.clientId")
    private String clientIdRequired;
    @Inject
    @ConfigProperty(name = "iua.code.auth.validator.exception.state")
    private String stateRequired;
    @Inject
    @ConfigProperty(name = "iua.code.auth.validator.exception.codeChallenge")
    private String codeChallengeRequired;
    @Inject
    @ConfigProperty(name = "iua.code.auth.validator.exception.redirectUri")
    private String redirectUriRequired;
    @Inject
    @ConfigProperty(name = "iua.code.auth.validator.exception.codeChallengeMethod")
    private String codeChallengeMethodRequired;
    @Inject
    @ConfigProperty(name = "iua.code.auth.validator.exception.badResponseType")
    private String badResponseType;
    @Inject
    @ConfigProperty(name = "iua.code.auth.validator.exception.badCodeChallengeMethod")
    private String badCodeChallengeMethod;
    @Inject
    @ConfigProperty(name = "iua.code.auth.validator.exception.badCodeChallengeFormat")
    private String badCodeChallengeFormat;

    @Override
    public String getBadCodeChallengeFormat() {
        return this.badCodeChallengeFormat;
    }

    @Override
    public String getBadCodeChallengeMethod() {
        return this.badCodeChallengeMethod;
    }

    @Override
    public String getBadResponseType() {
        return this.badResponseType;
    }

    @Override
    public String getUrlFormat() {
        return this.urlFormat;
    }

    @Override
    public String getRedirectUriValue() {
        return this.redirectUriValue;
    }

    @Override
    public String getBadClientName() {
        return this.badClientName;
    }

    @Override
    public String getResponseTypeRequired() {
        return this.responseTypeRequired;
    }

    @Override
    public String getClientIdRequired() {
        return this.clientIdRequired;
    }

    @Override
    public String getStateRequired() {
        return this.stateRequired;
    }

    @Override
    public String getCodeChallengeRequired() {
        return this.codeChallengeRequired;
    }

    @Override
    public String getRedirectUriRequired() {
        return this.redirectUriRequired;
    }

    @Override
    public String getCodeChallengeMethodRequired() {
        return this.codeChallengeMethodRequired;
    }
}
