package net.ihe.gazelle.iuasimulator.interlay.configuration;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

/**
 * @author Pierre-Marie VAN HOUTEGHEM, Claude LUSSEAU
 * @company KEREVAL
 * @project iua-simulator
 * @date 26/01/2023
 */
@ApplicationPath("/rest")
public class JaxRsActivator extends Application {

}

