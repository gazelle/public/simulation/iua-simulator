package net.ihe.gazelle.iuasimulator;

import io.restassured.RestAssured;

public class RestConfig {
    static void setGlobalITConfig() {
        RestAssured.baseURI = System.getProperty("it.base.uri", "http://localhost");
        RestAssured.port = Integer.parseInt(System.getProperty("it.base.port", "8080"));
        RestAssured.basePath = "/iua-simulator/rest";
    }
}
