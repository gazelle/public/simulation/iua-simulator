#!/bin/bash

# Colors
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

# Database management
if [ "${KC_DB_ENABLED:-true}" = "true" ]; then
    export PGPASSWORD=${KC_DB_PASSWORD}

    # Check if db host is reachable
    while ! pg_isready -h $KC_DB_URL_HOST --port ${KC_DB_URL_PORT} >/dev/null 2> /dev/null;
    do
      echo "Waiting 5s for database server to be reachable"
      sleep 5
    done
    echo -e "${GREEN}✔ Database server is accessible.${NC}"

    # Check if Keycloak database is created
    psql -h ${KC_DB_URL_HOST} --port ${KC_DB_URL_PORT} -U ${KC_DB_USERNAME} ${KC_DB_URL_DATABASE} -c '\q' 2>/dev/null
    if [ $? -ne 0 ]; then
        echo "Creating ${KC_DB_URL_DATABASE} database..."
        createdb -U ${KC_DB_USERNAME} --port ${KC_DB_URL_PORT} -h ${KC_DB_URL_HOST} -E UTF-8 ${KC_DB_URL_DATABASE}
    fi
fi
echo "Waiting 5s for database server to be ready"
sleep 5

# Start Keycloak in dev or prod mode
if [ "$DEV_MODE" == "true" ]; then
  /opt/keycloak/bin/kc.sh -v start-dev --import-realm --features 'dynamic-scopes' --log-level=TRACE --log="console,file"
else
  /opt/keycloak/bin/kc.sh -v start --import-realm --features 'dynamic-scopes'

fi